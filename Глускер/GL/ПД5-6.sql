		Администратор
		
	Добавление пользователя

CREATE OR REPLACE FUNCTION public.add_user(_username VARCHAR(20), _password VARCHAR(20), _role INTEGER)
RETURNS BOOLEAN AS 
$function$
	BEGIN
	    IF _username IS NULL THEN 
	        RAISE EXCEPTION 'USERNAME IS NULL';
	        RETURN false;
	    ELSIF _role IS NULL THEN 
	        RAISE EXCEPTION 'ROLE IS NULL';
	        RETURN false;
		ELSIF EXISTS ( SELECT username
					FROM public.users
					WHERE username = _username )
		THEN 
		    RAISE EXCEPTION 'USERNAME CONFLICT';
		    RETURN false;
		ELSE
		    INSERT INTO public.users (username, password, role_number)
		    VALUES (_username, _password, _role);
		    RETURN true;
		END IF;
	END;
$function$ 
LANGUAGE plpgsql

	Изменение пользователя
	
CREATE OR REPLACE FUNCTION public.modify_user(_username VARCHAR(20), 
											  _password VARCHAR(20), 
											  _role INTEGER)
RETURNS BOOLEAN AS 
$function$
	BEGIN
	IF _username IS NULL THEN 
	        RAISE EXCEPTION 'USERNAME IS NULL';
	        RETURN false;
	    ELSIF _role IS NULL THEN 
	        RAISE EXCEPTION 'ROLE IS NULL';
	        RETURN false;
		ELSE
		    UPDATE public.users
		    SET (Username, Password, Role_number) = (_username, _password, _role)
		    WHERE Username = _username;
		    RETURN true;
		END IF;
	END;
$function$ 
LANGUAGE plpgsql

	Удаление пользователя
	
CREATE OR REPLACE FUNCTION public.modify_user(_username character varying)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
	BEGIN
	IF _username IS NULL THEN 
	        RAISE EXCEPTION 'USERNAME IS NULL';
	        RETURN false;
		ELSE
		    DELETE FROM public.users 
		    WHERE Username = _username;
		    RETURN true;
		END IF;
	END;
$function$


-------------------------------------------------------------------------------------------

		Клиент
	
	Покупка билета
	
CREATE OR REPLACE FUNCTION public.add_user(_username character varying, _password character varying, _role integer)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
	BEGIN
	    IF _username IS NULL THEN 
	        RAISE EXCEPTION 'USERNAME IS NULL';
	        RETURN false;
	    ELSIF _role IS NULL THEN 
	        RAISE EXCEPTION 'ROLE IS NULL';
	        RETURN false;
		ELSIF EXISTS ( SELECT username
					FROM public.users
					WHERE username = _username )
		THEN 
		    RAISE EXCEPTION 'USERNAME CONFLICT';
		    RETURN false;
		ELSE
		    INSERT INTO public.users (username, password, role_number)
		    VALUES (_username, _password, _role);
		    RETURN true;
		END IF;
	END;
$function$


	Просмотр информации о рейсах
	
CREATE OR REPLACE FUNCTION public.show_flights()
RETURNS TABLE (flight_number INTEGER,
			   airplane_number INTEGER, 
			   departure_date DATETIME, 
			   date_of_arrival DATETIME, 
			   arrive_airport_number INTEGER, 
			   departure_airport_number INTEGER) AS 
$function$
	BEGIN
		RETURN QUERY SELECT * 
		FROM public.flights ;
	END;
$function$ 
LANGUAGE plpgsql

	Просмотр информации о своем билете
	
CREATE OR REPLACE FUNCTION public.show_my_ticket(_passenger_number INTEGER)
RETURNS TABLE (seat_number INTEGER, cost INTEGER, ticket_number INTEGER,
			   passenger_number INTEGER, flight_number INTEGER) AS 
$function$
	BEGIN
		RETURN QUERY SELECT * 
		FROM public.tickets
		WHERE Passenger_number = _passenger_number;
	END;
$function$ 
LANGUAGE plpgsql

	Возврат билета

CREATE OR REPLACE FUNCTION public.cancel_ticket(t_number INTEGER)
RETURNS void AS 
$function$
	BEGIN
		UPDATE public.tickets
		SET passenger_number = NULL
		WHERE ticket_number = t_number;
	END;
$function$ 
LANGUAGE plpgsql

