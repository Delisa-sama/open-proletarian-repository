unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, SynHighlighterCpp, RTTICtrls, RTTIGrids,
  PrintersDlgs, Forms, Controls, Graphics, Dialogs, ActnList, DBGrids, ValEdit,
  Grids, ColorBox, StdCtrls, ExtCtrls, crt, Menu, loops, sheets, types_,
  Printers, PaintPanel;

type

  { TForm1 }

  TForm1 = class(TForm)
    OpenDialog1: TOpenDialog;
    PaintPanel1: TPaintPanel;
    PrintDialog1: TPrintDialog;
    SaveDialog1: TSaveDialog;
    procedure FormDropFiles(Sender: TObject; FileNames: array of String);
    procedure ValueListEditor1Click(Sender: TObject);

  private
    { private declarations }
  public
    Label1: TLabel;
    Label2: TLabel;
    { public declarations }
  end;


  var
  Form1              : TForm1;
  menu_obj           : TMenu;
  items              : strings;
  str_tops           : arr_strings;
  array_of_columns   : arr_colms;
  array_of_types_spr : arr_types;          //1 - Date; 2 - Str; 3 - Code; 4 - Num; 5 - связанынй тип
  i                  : integer;
  arr_of_bonds_spr   : arr_bonds;
  num_of_bonds       : integer;

implementation


{$R *.lfm}

{ TForm1 }


{$define arr}

procedure TForm1.ValueListEditor1Click(Sender: TObject);

begin

    str_tops[1,1]:= ('Дата вылета');            //Date
    array_of_types_spr[1,1]:=Date;
    str_tops[1,2]:= ('Аэропорт вылета');               //Str
    array_of_types_spr[1,2]:=Str;
    str_tops[1,3]:= ('Аэропорт прилета');      //Str
    array_of_types_spr[1,3]:=Str;
    str_tops[1,4]:= ('Дата прилета');       //Date
    array_of_types_spr[1,4]:= Date;
    str_tops[1,5]:= ('Марка самолета');      //Str//
    array_of_types_spr[1,5]:=Str;
    str_tops[2,1]:= ('Код');          //Code
    array_of_types_spr[2,1]:=Code;
    str_tops[2,2]:= ('Город');       //Str
    array_of_types_spr[2,2]:=Str;
    str_tops[2,3]:= ('Название');        //Str//
    array_of_types_spr[2,3]:=Str;
    str_tops[3,1]:= ('Рейс');               //Code
    array_of_types_spr[3,1]:=Code;
    str_tops[3,2]:= ('Код кресла');            //Code
    array_of_types_spr[3,2]:=Code;
    str_tops[3,3]:= ('Стоимость');          //Num
    array_of_types_spr[3,3]:=Number;

    array_of_columns[1] := 5;
    array_of_columns[2] := 3;
    array_of_columns[3] := 3;

    num_of_bonds := 2;

    arr_of_bonds_spr[1].spr1 := 1;
    arr_of_bonds_spr[1].col1 := 2;
    arr_of_bonds_spr[1].spr2 := 2;
    arr_of_bonds_spr[1].col2 := 2;

    arr_of_bonds_spr[2].spr1 := 1;
    arr_of_bonds_spr[2].col1 := 3;
    arr_of_bonds_spr[2].spr2 := 2;
    arr_of_bonds_spr[2].col2 := 3;

    items[1] := 'Spreadsheet 1';
    items[2] := 'Spreadsheet 2';
    items[3] := 'Spreadsheet 3';

    menu_obj := TMenu.create( 3, items, str_tops, array_of_columns, array_of_types_spr, num_of_bonds, arr_of_bonds_spr, form1 );

end;

procedure TForm1.FormDropFiles(Sender: TObject; FileNames: array of String
  );
//var buf : string;
begin
  if menu_obj.load(filenames[0]) > 0 then begin
    for i := 1 to 3 do menu_obj.arr_Buttons[i].Enabled := FALSE;
    menu_obj.arr_Buttons[4].Enabled := FALSE;
  end
  else begin
    for i := 1 to 3 do menu_obj.arr_Buttons[i].Enabled := TRUE;
    menu_obj.arr_Buttons[4].Enabled := TRUE;
  end;
end;



begin
end.
