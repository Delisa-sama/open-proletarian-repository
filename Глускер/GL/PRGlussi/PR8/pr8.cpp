/*
	Удалить обычные файлы, которые в имени имеют
	хотя бы два символа, не являющиеся латиской буквой или цифрой.
*/

#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <dirent.h>
#include <unistd.h>

using namespace std;

int func(char* dirName, int min = 255){
	
	printf("%s\n","func begin");
	char path[256];
	char minName[256] = {'\0'};
	strcpy(path, dirName);
	
	DIR *d;
	struct dirent *dir;
	int fd[2];
	
	if(pipe(fd) == -1) {
        perror("pipe create");
        return -1;
    };
	
	if(write(fd[1],path,256) == -1) {
		perror("write pipe");
		return -1;
	};
	
	errno = 0;	
	int buffErr = errno;
	int count = 0;
	
	d = opendir(dirName);
	if (d){
					
		while((dir = readdir(d)) != NULL) {
			printf("file: %s\n", dir->d_name);	
			if((dir -> d_type == DT_DIR) && (strcmp( dir -> d_name, ".") != 0)
			   && (strcmp(dir -> d_name, "..") != 0)) {
				
				char newPath[256];
				strcpy (newPath,path);
				strcat (newPath, "/");
				strcat (newPath, dir->d_name);
				
				pid_t pID = fork();
				
				if (pID == 0) {
					count = 0;
					if (closedir(d) != 0) {
						printf("%s", strerror(errno));
						return 1;
					}
					if (d = opendir(newPath)) {
						strcpy(path, newPath);	
					}
				} else if(pID > 0){
					count++;
				} else {
					perror("");
					return 2;
				}
			} else if (dir -> d_type == DT_REG) {
				int dotPos = strlen(dir->d_name) - 1;
				while (dir->d_name[dotPos] != '.') {
					dotPos--;
				}
				if (dotPos <= 0) dotPos = 255;  
				printf("ext size:  %s: %d\n", dir->d_name, strlen(dir->d_name) - dotPos - 1);

				char buff[256] = {'\0'};
				if(read(fd[0], buff, 256) == -1) {
						perror("read pipe");
						return -1;
				};

				int dotPos_buff = strlen(buff) - 1;
				while (buff[dotPos_buff] != '.') {
					dotPos_buff--;
				}
				if (dotPos_buff <= 0) dotPos_buff = 255;
				printf("ext size:  %s: %d\n", buff, strlen(buff) - dotPos_buff - 1);
						

				if (strlen(dir->d_name) - dotPos < strlen(buff) - dotPos_buff - 1) {
					min = strlen(dir->d_name) - dotPos - 1;

					strcpy(minName,dir->d_name);
				}
				
				//min = strlen(dir->d_name) - dotPos - 1;
						
				//strcpy(minName,dir->d_name);
			}
		}		
			  
		if (buffErr != errno) {
			printf ("%s", strerror(errno));
			errno = 0;
		}
	}
	if (closedir(d) != 0){
		printf("%s",strerror(errno));
		return 3;	
	}	
	
	int stat;
	pid_t id;
	if(write(fd[1],minName,256) == -1) {
		perror("write pipe");
		return -1;
	};
	while((count > 0) && ((id = waitpid(0, &stat, 0)) > 0)){
			
			count--;
			
			if(!WIFEXITED(stat)) {
				perror("");				
				return 4;
			}
	}
	
	if (id < 0) {
		perror("");
		return 5;
	}	 
		
	return fd[0];
}


int main() {
	char b[256] = ".";
	char res[256] = {'\0'};
	printf("%s\n","func not begin");
	int fd = func(b);
	if(read(fd, res, 256) == -1) {
        perror("read result pipe");
    };
	if(close(fd) == -1) {
        perror("close result pipe");
        return -1;
    };
	printf("result: %s", strlen(res));
	return 0;
}