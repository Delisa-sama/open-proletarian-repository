
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <iostream>

void wait(int sec)
{

	boost::this_thread::sleep_for( boost::chrono::seconds{sec});
	
}

void thread()
{

	for (int i = 0; i < 5; ++i)
	{
	
		wait(2);
		std::cout << i << '\n';
	
	}
	
}

int main()
{

	boost::thread t{thread};
	t.join();

}