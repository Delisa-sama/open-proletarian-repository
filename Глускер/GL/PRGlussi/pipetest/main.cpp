#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <list>
#define MAX_LEN 16000
char* fun()
{
    DIR* dir = 0;

    if((dir = opendir("./")) == 0) {
        perror("open start dir");
        return 0;
    }

    struct dirent* file = 0;

    int master_pipe[2] = {0, 0};//главный пайп
    int derived_pipe[2] = {0, 0};//

    char* min_name = new char[MAX_LEN];
    char full_path[MAX_LEN] = {"./"};

    int p_count = 0;

    while((file = readdir(dir)) != 0) {
        if(file->d_type == DT_REG) {
            char* ptr = strrchr(file->d_name, '.');
            char* ptr2 = 0;

            if(ptr != 0)
                if((ptr2 = strrchr(min_name, '.')) == 0 && strlen(ptr) > 1)
                    strcpy(min_name, file->d_name);
                else if(strlen(ptr) < strlen(ptr2) && strlen(ptr) > 1)
                    strcpy(min_name, file->d_name);
        }

        if(file->d_type == DT_DIR && strcmp(file->d_name, ".") != 0 &&
           strcmp(file->d_name, "..") != 0) {
            int pid;

            if(!derived_pipe[0]) {
                pipe(derived_pipe);
            }

            pid = fork();

            if(!pid) {//child
                if(close(derived_pipe[0]) == -1) {
                    perror("close d_pipe[read] 1");
                    return 0;
                }

                master_pipe[0] = derived_pipe[0];
                master_pipe[1] = derived_pipe[1];

                derived_pipe[0] = 0;
                derived_pipe[1] = 0;

                char dir_path[MAX_LEN];

                strcpy(dir_path, file->d_name);
                if(closedir(dir) == -1) {
                    perror("closedir");
                    // return 0;
                }

                strcat(full_path, dir_path);
                strcat(full_path, "/");

                if((dir = opendir(full_path)) == 0) {
                    perror("open new dir");
                    return 0;
                }

                p_count = 0;
            }  // derived
            else {
                p_count++;
            }  // parent
        }
    }

    if(closedir(dir) == -1) {
        perror("closedir");
        return 0;
    }

    int stat;
    pid_t id;

    while((p_count != 0) && ((id = waitpid(0, &stat, 0)) > 0)) {
        p_count--;

        if(!WIFEXITED(stat)) {
            perror("Proccess error");
            return 0;
        }
    }

    if(close(derived_pipe[1]) == -1) {
        perror("close d_pipe[write]");
        return 0;
    }
    derived_pipe[1] = 0;

    char buf_str[MAX_LEN] = {"\0"};
    char* buf_ptr = &(buf_str[0]);

    if(derived_pipe[0])
        while(read(derived_pipe[0], buf_ptr, 1)) {
            if(*buf_ptr == '\0') {
                buf_ptr = &(buf_str[0]);
                char* ptr = strrchr(buf_str, '.');
                char* ptr2 = 0;

                if(ptr != 0)
                    if((ptr2 = strrchr(min_name, '.')) == 0 && strlen(ptr) > 1)
                        strcpy(min_name, buf_str);
                    else if(strlen(ptr) < strlen(ptr2) && strlen(ptr) > 1)
                        strcpy(min_name, buf_str);
            } else
                buf_ptr++;
        }

    if(master_pipe[1]) {
        // printf("%d\n", write(m_pipe[1], min_name, strlen(min_name) + 1));
        write(master_pipe[1], min_name, strlen(min_name) + 1);
        if(close(master_pipe[1]) == -1) {
            perror("close m_pipe[write]");
            return 0;
        }
        if(derived_pipe[0])
            if(close(derived_pipe[0]) == -1) {
                perror("close d_pipe[read] 2");
                return 0;
            }

        if(min_name)
            delete[] min_name;

        return 0;
    }

    if(close(master_pipe[1]) == -1) {
        perror("close m_pipe[write]");
        return 0;
    };

    if(close(derived_pipe[0]) == -1) {
        perror("close d_pipe[read] 3");
        return 0;
    };

    return min_name;
}

int main()
{
    char* str = fun();

    if(str != 0) {
        printf("result %s\n", str);
        delete[] str;
    }

    return 0;
}
