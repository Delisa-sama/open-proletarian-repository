unit Types_;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, SynHighlighterCpp, RTTICtrls, RTTIGrids, Forms,
  Controls, Graphics, Dialogs, ActnList, DBGrids, ValEdit, Grids, ColorBox,
  StdCtrls, ExtCtrls,crt;

type
  bond        = record
    spr1 : integer;
    col1 : integer;

    spr2 : integer;
    col2 : integer;
  end;


type

  types       = (Date,Code,Number,Str);
    strings     = array [1..10] of string;
    arr_colms   = array [1..10] of integer;
    arr_types   = array [1..3]  of array [1..10] of types;
    arr_types_1 = array [1..10] of types;
    arr_strings = array [1..10] of strings;
    arr_bonds   = array [1..100] of bond;

implementation

end.

