unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls, Grids, IniFiles;

type

  { TWDB }

  TWDB = class(TForm)
    AddButton: TButton;
    AddToTableButton: TButton;
    SettingsButton: TButton;
    FindBox: TGroupBox;
    OpDi: TOpenDialog;
    FindAddToTableButton: TButton;
    FindDeleteButton: TButton;
    FindEnterText: TEdit;
    SvDi: TSaveDialog;
    ScndAddToTableBUtton: TButton;
    SearchEdit: TEdit;
    ScndSearchEdit: TEdit;
    ScndAddButton: TButton;
    ScndDeleteButton: TButton;
    DeleteButton: TButton;
    ScndEntertext: TEdit;
    ScndSearchButton: TButton;
    SortButton: TButton;
    SearchButton: TButton;
    Entertext: TEdit;
    ScndSortButton: TButton;
    ScndTable: TStringGrid;
    FindTable: TStringGrid;
    TableBox: TGroupBox;
    LoadButton: TButton;
    SaveButton: TButton;
    OpenTableButton: TButton;
    OpenScndTableButton: TButton;
    MenuButtonBox: TGroupBox;
    Table: TStringGrid;
    ScndTableBox: TGroupBox;
    procedure AddButtonClick(Sender: TObject);
    procedure AddToTableButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
    procedure EntertextClick(Sender: TObject);
    procedure FindAddToTableButtonClick(Sender: TObject);
    procedure FindDeleteButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of string);
    procedure LoadButtonClick(Sender: TObject);
    procedure OpenScndTableButtonClick(Sender: TObject);
    procedure OpenTableButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure ScndAddButtonClick(Sender: TObject);
    procedure ScndAddToTableBUttonClick(Sender: TObject);
    procedure ScndDeleteButtonClick(Sender: TObject);
    procedure ScndEntertextClick(Sender: TObject);
    procedure ScndSearchButtonClick(Sender: TObject);
    procedure ScndSortButtonClick(Sender: TObject);
    procedure ScndTableDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure ScndTableDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure ScndTableMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure ScndTableSelectCell(Sender: TObject; aCol, aRow: integer;
      var CanSelect: boolean);
    procedure SearchButtonClick(Sender: TObject);
    procedure SettingsButtonClick(Sender: TObject);
    procedure SortButtonClick(Sender: TObject);
    procedure TableDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure TableDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure TableMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure TableSelectCell(Sender: TObject; aCol, aRow: integer;
      var CanSelect: boolean);
  private
    { private declarations }
  public
    arr_of_keys: array [1..100] of integer;
    { public declarations }
  end;

var
  WDB: TWDB;

implementation

{$R *.lfm}

{ TWDB }

procedure TWDB.LoadButtonClick(Sender: TObject);
var
  untfile: file;
  i, n, j, l, buff: integer;
  f: char;
  filename: string;
  err: word;
begin
  if OpDi.Execute then
  begin
    filename := OpDi.Filename;
    for n := 1 to 6 do
      for l := 1 to Table.RowCount - 1 do
        Table.Cells[n, l] := '';
    for n := 1 to 3 do
      for l := 1 to ScndTable.RowCount - 1 do
        ScndTable.Cells[n, l] := '';
    AssignFile(untfile, filename);
    Reset(untfile, 1);
      {$I-}
    blockread(untfile, buff, sizeof(buff));
      {$I+}
    err := ioresult;
    Table.RowCount := Buff;
    for n := 1 to Table.RowCount - 1 do
      for l := 1 to 6 do
      begin
          {$I-}
        blockread(untfile, j, 2);
          {$I+}
        err := ioresult;
        for i := 1 to j do
        begin
            {$I-}
          blockread(untfile, f, 1);
            {$I+}
          err := ioresult;
          if (err <> 0) then
            ShowMessage('Некорректная загрузка')
          else
            Table.Cells[l, n] := Table.Cells[l, n] + f;
        end;
      end;
      {$I-}
    blockread(untfile, buff, 2);
    err := ioresult;
      {$I+}
    ScndTable.RowCount := buff;
    for n := 1 to buff - 1 do
      for l := 1 to 3 do
      begin
          {$I-}
        blockread(untfile, j, 2);
        err := ioresult;
          {$I+}
        for i := 1 to j do
        begin
            {$I-}
          blockread(untfile, f, 1);
          err := ioresult;
            {$I+}
          if (err <> 0) then
            ShowMessage('Некорректная загрузка')
          else
            ScndTable.Cells[l, n] := ScndTable.Cells[l, n] + f;
        end;
      end;
    CloseFile(untfile);
  end;
end;

procedure TWDB.AddButtonClick(Sender: TObject);
begin
  if Table.RowCount < 100 then
  begin
    Table.RowCount := Table.RowCount + 1;
  end;
end;

procedure TWDB.AddToTableButtonClick(Sender: TObject);
begin
  if (Entertext.Text > '0') then
    Table.Cells[Table.Col, Table.Row] := Entertext.Text
  else if table.col <> 5 then
    Table.Cells[Table.Col, Table.Row] := Entertext.Text;
end;

procedure TWDB.DeleteButtonClick(Sender: TObject);
begin
  if Table.RowCount > 2 then
    Table.DeleteRow(Table.Row);
end;

procedure TWDB.EntertextClick(Sender: TObject);
begin
  Entertext.Text := '';
end;

procedure TWDB.FindAddToTableButtonClick(Sender: TObject);
begin
  FindTable.Cells[FindTable.Col, FindTable.Row] := FindEntertext.Text;
  if FindEntertext.tag = 1 then
  begin
    Table.Cells[FindTable.Col, arr_of_keys[FindTable.Row]] :=
      FindTable.Cells[FindTable.Col, FindTable.Row];
  end
  else if FindEntertext.Tag = 2 then
  begin
    ScndTable.Cells[FindTable.Col, arr_of_keys[FindTable.Row]] :=
      FindTable.Cells[FindTable.Col, FindTable.Row];
  end;
end;

procedure TWDB.FindDeleteButtonClick(Sender: TObject);
begin
  FindTable.DeleteRow(FindTable.Row);
  if FindEntertext.tag = 1 then
  begin
    Table.DeleteRow(arr_of_keys[FindTable.Row]);
  end
  else if FindEntertext.Tag = 2 then
  begin
    ScndTable.DeleteRow(arr_of_keys[FindTable.Row]);
  end;
end;

procedure TWDB.FormCreate(Sender: TObject);
const SECTION = 'Colors';
var INI: TINIFile;
    TableColor: string;
begin

  INI := TINIFile.create('config.ini');

  try

    TableColor := INI.ReadString(SECTION, 'TableColor','');

    TableBox.Color:=StringToColor(TableColor);
    ScndTableBox.Color:=StringToColor(TableColor);
    FindBox.Color:=StringToColor(TableColor);

  finally

    INI.free;

  end;

end;


procedure TWDB.FormDropFiles(Sender: TObject; const FileNames: array of string);
var
  untfile: file;
  i, n, j, l, buff: integer;
  f: char;
  err: word;
begin
  if FileNames[0] <> '' then
  begin
    for n := 1 to 6 do
      for l := 1 to Table.RowCount - 1 do
        Table.Cells[n, l] := '';
    for n := 1 to 3 do
      for l := 1 to ScndTable.RowCount - 1 do
        ScndTable.Cells[n, l] := '';
    AssignFile(untfile, FileNames[0]);
    Reset(untfile, 1);
      {$I-}
    blockread(untfile, buff, sizeof(buff));
      {$I+}
    err := ioresult;
    Table.RowCount := Buff;
    for n := 1 to Table.RowCount - 1 do
      for l := 1 to 6 do
      begin
          {$I-}
        blockread(untfile, j, 2);
          {$I+}
        err := ioresult;
        for i := 1 to j do
        begin
            {$I-}
          blockread(untfile, f, 1);
            {$I+}
          err := ioresult;
          if (err <> 0) then
            ShowMessage('Некорректная загрузка')
          else
            Table.Cells[l, n] := Table.Cells[l, n] + f;
        end;
      end;
      {$I-}
    blockread(untfile, buff, 2);
    err := ioresult;
      {$I+}
    ScndTable.RowCount := buff;
    for n := 1 to buff - 1 do
      for l := 1 to 3 do
      begin
          {$I-}
        blockread(untfile, j, 2);
        err := ioresult;
          {$I+}
        for i := 1 to j do
        begin
            {$I-}
          blockread(untfile, f, 1);
          err := ioresult;
            {$I+}
          if (err <> 0) then
            ShowMessage('Некорректная загрузка')
          else
            ScndTable.Cells[l, n] := ScndTable.Cells[l, n] + f;
        end;
      end;
    CloseFile(untfile);
  end;
end;

procedure TWDB.OpenScndTableButtonClick(Sender: TObject);
var
  i, j: integer;
begin
  if (TableBox.Enabled <> False) and (TableBox.Visible <> False) then
  begin
    TableBox.Visible := False;
    TableBox.Enabled := False;
  end;
  ScndTableBox.Visible := True;
  ScndTableBox.Enabled := True;

  if FindTable.Visible = True then
  begin
    for i := 1 to FindTable.ColCount - 1 do
      for j := 1 to FindTable.RowCount - 1 do
      begin
        arr_of_keys[j] := 0;
        FindTable.Cells[i, j] := '';
      end;
    FindBox.Visible := False;
    FindBox.Enabled := False;
  end;
end;

procedure TWDB.OpenTableButtonClick(Sender: TObject);
var
  i, j: integer;
begin
  if (ScndTableBox.Enabled <> False) and (ScndTableBox.Visible <> False) then
  begin
    ScndTableBox.Visible := False;
    ScndTableBox.Enabled := False;
  end;
  TableBox.Visible := True;
  TableBox.Enabled := True;

  if FindTable.Visible = True then
  begin
    for i := 1 to FindTable.ColCount - 1 do
      for j := 1 to FindTable.RowCount - 1 do
      begin
        arr_of_keys[j] := 0;
        FindTable.Cells[i, j] := '';
      end;
    FindBox.Visible := False;
    FindBox.Enabled := False;
  end;
end;


procedure TWDB.SaveButtonClick(Sender: TObject);
var
  untfile: file;
  filename: string;
  i, n, j, f: integer;
  buff: string;
begin
  if SvDi.Execute then
  begin
    filename := SvDi.Filename;
    assignfile(untfile, filename);
    rewrite(untfile, 1);
    blockwrite(untfile, Table.RowCount, sizeof(Table.RowCount));
    for n := 1 to Table.RowCount - 1 do
      for f := 1 to 6 do
      begin
        j := length(Table.Cells[f, n]);
        blockwrite(untfile, j, 2);
        buff := Table.Cells[f, n];
        for i := 1 to j do
          blockwrite(untfile, buff[i], 1);
      end;
    blockwrite(untfile, ScndTable.RowCount, 2);
    for n := 1 to ScndTable.RowCount - 1 do
      for f := 1 to 3 do
      begin
        j := length(ScndTable.Cells[f, n]);
        blockwrite(untfile, j, 2);
        buff := ScndTable.Cells[f, n];
        for i := 1 to j do
          blockwrite(untfile, buff[i], 1);
      end;
    closefile(untfile);
  end;
end;

procedure TWDB.ScndAddButtonClick(Sender: TObject);
begin
  if ScndTable.RowCount < 100 then
  begin
    ScndTable.RowCount := ScndTable.RowCount + 1;
  end;
end;

procedure TWDB.ScndAddToTableButtonClick(Sender: TObject);
begin
  case ScndTable.col of
    2: if (ScndEntertext.Text > '1900') and (ScndEntertext.Text <= '2017') then
        ScndTable.Cells[ScndTable.Col, ScndTable.Row] := ScndEntertext.Text;
    3: if (ScndEntertext.Text = 'мужской') or (ScndEntertext.Text = 'женский') then
        ScndTable.Cells[ScndTable.Col, ScndTable.Row] := ScndEntertext.Text;
    else
      ScndTable.Cells[ScndTable.Col, ScndTable.Row] := ScndEntertext.Text;
  end;
end;

procedure TWDB.ScndDeleteButtonClick(Sender: TObject);
begin
  if ScndTable.RowCount > 2 then
    ScndTable.DeleteRow(ScndTable.Row);
end;


procedure TWDB.ScndEntertextClick(Sender: TObject);
begin
  ScndEntertext.Text := '';
end;

procedure TWDB.ScndSearchButtonClick(Sender: TObject);
var
  FlagFind: boolean = False;
  i, j, x, nowRow: integer;
begin
  FindBox.Enabled := True;
  FindBox.Visible := True;
  FindEnterText.Tag := 2;
  nowRow := 1;
  FindTable.ColCount := ScndTable.ColCount;
  FindTable.RowCount := ScndTable.RowCount;
  if FindTable <> nil then
  begin
    for i := 1 to FindTable.ColCount - 1 do
      for j := 1 to FindTable.RowCount - 1 do
      begin
        FindTable.Cells[i, j] := '';
      end;
  end;
  for i := 1 to FindTable.ColCount - 1 do
    FindTable.Cells[i, 0] := ScndTable.Cells[i, 0];
  for i := 1 to ScndTable.RowCount - 1 do
  begin
    flagFind := False;
    j := 1;
    while (j <= ScndTable.ColCount - 1) and not (FlagFind) do
    begin
      if (pos(ScndSearchEdit.Text, ScndTable.Cells[j, i]) > 0) and
        (i <= ScndTable.RowCount - 1) then
      begin
        arr_of_keys[nowRow] := i;
        FlagFind := True;
        FindTable.Visible := True;
        FindTable.Enabled := True;
        for x := 1 to ScndTable.ColCount - 1 do
          FindTable.Cells[x, nowRow] := ScndTable.Cells[x, i];
        Inc(nowRow);
      end;
      Inc(j);
    end;
  end;
end;

procedure TWDB.ScndSortButtonClick(Sender: TObject);
var
  i, j, f, x: integer;
  s: array[1..3] of string;
begin
  for f := 1 to ScndTable.ColCount - 1 do
    for i := 1 to ScndTable.RowCount - 1 do
      for j := 1 to (ScndTable.RowCount - i) - 1 do
        if ScndTable.Cells[f, j] > ScndTable.Cells[f, j + 1] then
        begin
          for x := 1 to ScndTable.ColCount - 1 do
            s[x] := ScndTable.Cells[x, j];
          for x := 1 to ScndTable.ColCount - 1 do
            ScndTable.Cells[x, j] := ScndTable.Cells[x, j + 1];
          for x := 1 to ScndTable.ColCount - 1 do
            ScndTable.Cells[x, j + 1] := s[x];
        end;
end;

procedure TWDB.ScndTableDragDrop(Sender, Source: TObject; X, Y: integer);
var
  ax: integer = 0;
  ay: integer = 0;
begin
  if Source = Sender then
  begin
    ScndTable.MouseToCell(X, Y, ay, ax);
    if ax <> 0 then
      ScndTable.MoveColRow(False, Table.row, ax);
  end;
end;

procedure TWDB.ScndTableDragOver(Sender, Source: TObject; X, Y: integer;
  State: TDragState; var Accept: boolean);
begin
  Accept := True;
end;

procedure TWDB.ScndTableMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  if Button = mbLeft then
    ScndTable.BeginDrag(False, 4);
end;

procedure TWDB.ScndTableSelectCell(Sender: TObject; aCol, aRow: integer;
  var CanSelect: boolean);
begin
  ScndEnterText.Text := ScndTable.Cells[aCol, aRow];
end;

procedure TWDB.SearchButtonClick(Sender: TObject);
var
  FlagFind: boolean = False;
  i, j, x, nowRow: integer;
begin
  FindBox.Enabled := True;
  FindBox.Visible := True;
  FindEnterText.Tag := 1;
  nowRow := 1;
  FindTable.ColCount := Table.ColCount;
  FindTable.RowCount := Table.RowCount;
  if FindTable <> nil then
  begin
    for i := 1 to FindTable.ColCount - 1 do
      for j := 1 to FindTable.RowCount - 1 do
      begin
        FindTable.Cells[i, j] := '';
      end;
  end;
  for i := 1 to FindTable.ColCount - 1 do
    FindTable.Cells[i, 0] := Table.Cells[i, 0];
  for i := 1 to Table.RowCount - 1 do
  begin
    flagFind := False;
    j := 1;
    while (j <= Table.ColCount - 1) and not (FlagFind) do
    begin
      if (pos(SearchEdit.Text, Table.Cells[j, i]) > 0) and (i <= Table.RowCount - 1) then
      begin
        arr_of_keys[nowRow] := i;
        FlagFind := True;
        FindTable.Visible := True;
        FindTable.Enabled := True;
        for x := 1 to Table.ColCount - 1 do
          FindTable.Cells[x, nowRow] := Table.Cells[x, i];
        Inc(nowRow);
      end;
      Inc(j);
    end;
  end;
end;

procedure TWDB.SettingsButtonClick(Sender: TObject);
const SECTION = 'Colors';
var F: TForm;
    B: TButton;
    ColorDialog: TColorDialog;
    INI: TINIFile;
begin

  ColorDialog := TColorDialog.Create(self);

  if ColorDialog.Execute then begin
    TableBox.color := ColorDialog.Color;
    ScndTableBox.color := ColorDialog.Color;
    FindBox.color := ColorDialog.Color;
  end;

  INI := TINIFile.create('config.ini');

  try
    INI.WriteString(SECTION, 'TableColor', ColorToString(ColorDialog.Color));

  finally

    INI.free;

  end;

  ColorDialog.Destroy;

end;

procedure TWDB.SortButtonClick(Sender: TObject);
var
  i, j, f, x: integer;
  s: array[1..6] of string;
begin
  for f := 1 to Table.ColCount - 1 do
    for i := 1 to Table.RowCount - 1 do
      for j := 1 to (Table.RowCount - i) - 1 do
        if Table.Cells[f, j] > Table.Cells[f, j + 1] then
        begin
          for x := 1 to Table.ColCount - 1 do
            s[x] := Table.Cells[x, j];
          for x := 1 to Table.ColCount - 1 do
            Table.Cells[x, j] := Table.Cells[x, j + 1];
          for x := 1 to Table.ColCount - 1 do
            Table.Cells[x, j + 1] := s[x];
        end;
end;

procedure TWDB.TableDragDrop(Sender, Source: TObject; X, Y: integer);
var
  ax: integer = 0;
  ay: integer = 0;
begin
  if Source = Sender then
  begin
    Table.MouseToCell(X, Y, ay, ax);
    if (ax <> 0) and (ay <> 0) then
      Table.MoveColRow(False, Table.row, ax)
    else Table.EndDrag(true);
  end;
end;

procedure TWDB.TableDragOver(Sender, Source: TObject; X, Y: integer;
  State: TDragState; var Accept: boolean);
begin  Accept := true;
end;

procedure TWDB.TableMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  if (Button = mbLeft) and (x <> 0) and (y <> 0) then
    Table.BeginDrag(False, 4);
end;

procedure TWDB.TableSelectCell(Sender: TObject; aCol, aRow: integer;
  var CanSelect: boolean);
begin
  EnterText.Text := Table.Cells[aCol, aRow];
end;

end.
