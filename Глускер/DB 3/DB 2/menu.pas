unit Menu;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Loops,sheets,StdCtrls, crt,types_,Forms,Dialogs, INIfiles, DOM, XMLWrite, XMLRead;
type
  //����� ������ ����
  TItem = class
    strict private
      Spr_loop : TLoop;
    public
      Spreadsheet : TSpreadsheet;
      procedure execute;
      procedure SetItemName ( var s : string );
      constructor create ( var s : string; num_of_columns : integer; tops : strings; var array_of_types_spr : arr_types_1; form : TForm );
      function getName : String;
      destructor Free_Item;
  end;

  //������������� �� ����� �������
  //����� ���������, �������� ������ ����(Item)
  TItems = class
  strict private
    Count_of_Items: integer;
  public
    Array_of_Items: array [1..3] of tItem;
    procedure execute (number:integer);
    function getItemName(num:integer):String;
    destructor Free_Container;
    constructor Create_Container (var s:strings; num:integer; str_of_tops: arr_strings; var num_of_columns:arr_colms; var array_of_types_spr:arr_types;num_of_bonds:integer; arr_of_bonds : arr_bonds; form : TForm);
    function getCountOfItems ():integer;
  //Procedure setCountOfItems (num:integer);
  end;

  //����� ���������� �� ��� ����������� �����
  TGUI = class
  strict private
    position: integer;
    procedure SetDedicatedItem(container:titems);
    procedure SetDefaultItem(container:titems);
  public
    procedure DrawMenu(container:tItems);
    procedure SetUpperPosition(container:titems);
    procedure SetLowerPosition(container:titems);
    function GetPosition: integer;
    function GetCoord: integer;
    constructor create (container:titems);
  end;

  //�������� ����� ����
  TMenu = class//(TLoopInterface)
  strict private
    MenuGUI     : TGUI;
    //arr_Buttons : array[1..10] of TButton;
    form        : TForm;
    opend       : TOpenDialog;
    saved       : TSaveDialog;
  public
    arr_Buttons : array[1..10] of TButton;
    Container : TItems;

    constructor create ( num : integer; items_of_menu : strings; str_of_tops : arr_strings; num_of_columns : arr_colms; var array_of_types_spr:arr_types;num_of_bonds:integer; arr_of_bonds:arr_bonds; form1 : TForm);
    procedure ReadColors ( filename : string);
    procedure Redirection ( sender : TObject );// override;
    procedure MenuDisplaying;
    procedure ShowError ( ErrorCode : word );
    procedure save;
    function load ( filename : string ): integer;
  end;

  implementation
                                     //////    TItem    //////

  //
  procedure TItem.execute;
  begin
    Spreadsheet.display;
    Spr_loop.loop;
  end;

  //����������� ������ ����
  constructor TItem.create ( var s : string; num_of_columns : integer; tops : strings;
  var array_of_types_spr : arr_types_1; form : TForm );
  begin
    Spreadsheet := TSpreadsheet.create ( tops, num_of_columns, array_of_types_spr, form, FALSE );
    Spr_loop := TLoop.create ( Spreadsheet );
  end;

  //��������, ���������� Name
  function TItem.GetName:string;
  begin
    //GetName := Name;    click
  end;

  //��������, ������������� Name
  procedure TItem.SetItemName(var s:string);
  begin
    //Name := s;
  end;

  //����������
  destructor TItem.Free_Item;
  begin

  end;

                                            //////    TItems    //////

  //
  procedure TItems.execute (number:integer);
  begin
    Array_of_Items[number].execute;
  end;

  //��������, ���������� ���������� ������� ����
  function TItems.getCountOfItems ():integer;
  begin
    getCountOfItems := Count_of_Items;
  end;

  //����������� ���������� ��� �������� ������� ����
  constructor TItems.Create_Container (var s:strings; num:integer; str_of_tops: arr_strings; var num_of_columns:arr_colms;var array_of_types_spr:arr_types;num_of_bonds:integer; arr_of_bonds : arr_bonds; form : TForm );
  var i:integer;
  begin
    Count_of_Items := num;
    for i:=1 to Count_of_Items do
      Array_of_items[i] := titem.create(s[i], num_of_columns[i], str_of_tops[i], array_of_types_spr[i], form );

    for i:= 1 to num_of_bonds do begin
      Array_of_items[arr_of_bonds[i].spr1].Spreadsheet.arr_of_bonds_col[arr_of_bonds[i].col1] := true;
      Array_of_items[arr_of_bonds[i].spr1].Spreadsheet.arr_of_spr_ptr[arr_of_bonds[i].col1] := array_of_items[arr_of_bonds[i].spr2].Spreadsheet;
      Array_of_items[arr_of_bonds[i].spr1].Spreadsheet.num_of_bonded_col[arr_of_bonds[i].col1] := arr_of_bonds[i].col2;
    end;

  end;

  //�������� ��� ��������� ����� ������ ���� �� ������ � ����������.
  function TItems.getItemName (num:integer):String;
  begin
    getItemName := Array_of_Items[num].getName;
  end;

  //����������
  destructor TItems.Free_Container;
  begin

  end;

                                               //////    TGUI    //////

  //������ ���������� ������
  procedure TGUI.SetDedicatedItem(container:titems);
  begin
    TextColor(10);
    gotoxy(51,GetCoord);
    write( Container.getItemName(GetPosition));
  end;

  //������ ������������ ������
  procedure TGUI.SetDefaultItem(container:titems);
  begin
    TextColor(White);
    gotoxy(51,GetCoord);
    write(Container.getItemName(GetPosition));
  end;

  //������ ����
  Procedure TGUI.DrawMenu(container:tItems);
  var y,x,i:integer;
  begin
    TextColor(white);
    y:=10;
    x:=45;
    gotoxy(x,y);
    write('#-------------------#');
    for i:=1 to container.getCountOfItems do begin
      inc(y);
      gotoxy(x,y);
      write('|');
      gotoxy(x+6,y);
      write(Container.getItemName(i));
      gotoxy(x+20,y);
      write('|');
    end;
    inc(y);
    gotoxy(x,y);
    write('#-------------------#');
    SetDedicatedItem(container);
    TextColor(white);
  end;

  //����������� ���
  constructor TGUI.create(container:titems);
  begin
    cursoroff;
    position := 1;
  end;

  //���������� ��������� �� ������ ���� ��������
  procedure TGUI.SetLowerPosition(container:titems);
  begin
    SetDefaultItem(container);
    if position = 3 then position:=1
    else inc(position);
    SetDedicatedItem(container);
    TextColor(white);
  end;

  //���������, ���������� �������
  function TGUI.GetPosition: integer;
  begin
    GetPosition:=position;
  end;

  //��������, ���������� ���������� Y
  function TGUI.GetCoord : integer;
  begin
    GetCoord := position + 10;
  end;

  //���������� ��������� �� ������ ���� ��������
  procedure TGUI.SetUpperPosition ( container : TItems );
  begin
    SetDefaultItem ( container );
    if position = 1 then position := 3
    else  dec ( position );
    SetDedicatedItem ( container );
    TextColor ( white );
  end;

                                                    //////    TMenu    //////

  //����������� ����
  constructor TMenu.create ( num : integer; items_of_menu : strings; str_of_tops : arr_strings;
    num_of_columns : arr_colms; var array_of_types_spr : arr_types; num_of_bonds : integer; arr_of_bonds : arr_bonds; form1 : TForm );
  var i   : integer;
      buf : string;
  begin
    opend := TOpenDialog.create ( form1 );
    saved := TSaveDialog.create ( form1 );

    form := form1;

    for i := 1 to num do begin
      arr_Buttons[i] := TButton.Create( form );
      arr_Buttons[i].Parent := form;
      arr_Buttons[i].left := 8;
      arr_Buttons[i].AutoSize := TRUE;
      arr_Buttons[i].top := 50 + 30 * ( i - 1 );
      arr_Buttons[i].Caption := items_of_menu[i];
      arr_Buttons[i].Tag := i;
      arr_Buttons[i].OnClick := @redirection;
    end;

    arr_Buttons[i + 3] := TButton.Create( form );
    arr_Buttons[i + 3].Parent := form;
    arr_Buttons[i + 3].left := 8;
    arr_Buttons[i + 3].top := 50 + 30 * ( i );
    buf := 'Update';
    arr_Buttons[i + 3].Caption := buf;
    arr_Buttons[i + 3].Tag := i + 3;
    arr_Buttons[i + 3].OnClick := @redirection;

    arr_Buttons[i + 1] := TButton.Create( form );
    arr_Buttons[i + 1].Parent := form;
    arr_Buttons[i + 1].left := 8;
    arr_Buttons[i + 1].top := 250;
    buf := 'Save';
    arr_Buttons[i + 1].Caption := buf;
    arr_Buttons[i + 1].Tag := i + 1;
    arr_Buttons[i + 1].OnClick := @redirection;

    arr_Buttons[i + 2] := TButton.Create( form );
    arr_Buttons[i + 2].Parent := form;
    arr_Buttons[i + 2].left := 8;
    arr_Buttons[i + 2].top := 280;
    buf := 'Load';
    arr_Buttons[i + 2].Caption := buf;
    arr_Buttons[i + 2].Tag := i + 2;
    arr_Buttons[i + 2].OnClick := @redirection;

    arr_Buttons[i + 4] := TButton.Create( form );
    arr_Buttons[i + 4].Parent := form;
    arr_Buttons[i + 4].left := 8;
    arr_Buttons[i + 4].top := 310;
    buf := 'Load ini file';
    arr_Buttons[i + 4].Caption := buf;
    arr_Buttons[i + 4].Tag := i + 4;
    arr_Buttons[i + 4].AutoSize := true;
    arr_Buttons[i + 4].OnClick := @redirection;


    Container := TItems.Create_Container(items_of_menu,num,str_of_tops, num_of_columns,array_of_types_spr,num_of_bonds, arr_of_bonds, form);
    MenuGUI := TGUI.create(container);
  end;

  //��������� ������ ������
procedure TMenu.ShowError(ErrorCode:word);
begin
  gotoxy(10,10);
  textcolor(red);
  case ErrorCode of
    2 :write('���� �� ������');
    100 :write('������ ������ � �����');
    101 :write('������ ������ �� ����');
    103 :write('���� �� ������');
  end;
  textcolor(white);
  delay(500);
  gotoxy(10,10);
  write('                     ');
end;

 //��������� �������� �� ����������� �����
function TMenu.Load (filename : string) : integer;
var i,buf2,j,k     : integer;
    coi            :integer = 3;
    f              : file;
    error          : word = 0;
    s, XMLstr      : string;
    str            : strings;
    buf            : Trow;
    doc            :TXMLDocument;
    Child          :TDOMNode;

begin
  //if opend.Execute then begin
    doc := TXMLDocument.create;
    ReadXMLFile(doc, FileName);

    Child := doc.FirstChild;
    s := Child.FirstChild.NextSibling.NodeName;
    for i := 1 to coi do begin
      Container.Array_of_Items[i].spreadsheet.num_of_rows := strtoint(Child.FirstChild.FirstChild.TextContent);

      for buf2 := 1 to Container.Array_of_Items[i].spreadsheet.num_of_rows do begin
        if ( Container.Array_of_Items[i].spreadsheet.getrow ( buf2 ) = NIL ) then begin
          for j := 1 to Container.Array_of_Items[i].spreadsheet.getcolumns do str [j] := ' ';
            buf := TRow.create( str, Container.Array_of_Items[i].spreadsheet.getColumns, Container.Array_of_Items[i].spreadsheet.array_of_types_spr, false );
            Container.Array_of_Items[i].spreadsheet.setRow( buf2, buf );
          end;
        end;
      error := Container.Array_of_Items[i].Spreadsheet.Load(doc, Child);
      child := child.FirstChild.NextSibling;
    end;


  //end;
  //  s := FileName;
  //  Assign ( f, s );
  //  {$I-}
  //  Reset(f,1);
  //  blockread(f,coi,sizeof(coi));
  //  {$I+}
  //  error := IOResult;
  //  if ( error = 0 ) and (( coi > 3 ) or ( coi < 0 )) then inc ( error )
  //  else
  //    for i := 1 to coi do begin
  //      {$I-}
  //      blockread(f,k,sizeof(Container.Array_of_Items[i].Spreadsheet.GetNumOfRows()));
  //      {$I+}
  //      error := ioresult;
  //      if error = 0 then begin
  //      Container.Array_of_Items[i].spreadsheet.num_of_rows := k;
  //      for buf2 := 1 to k do begin
  //        //Container.Array_of_Items[i].Spreadsheet.AddRow(Container.Array_of_Items[i].Spreadsheet.array_of_types_spr);
  //          if ( Container.Array_of_Items[i].spreadsheet.getrow ( buf2 ) = NIL ) then begin
  //            for j := 1 to Container.Array_of_Items[i].spreadsheet.getcolumns do str [j] := ' ';
  //            buf := TRow.create( str, Container.Array_of_Items[i].spreadsheet.getColumns, Container.Array_of_Items[i].spreadsheet.array_of_types_spr, false );
  //            Container.Array_of_Items[i].spreadsheet.SetRow( buf2, buf );
  //          end;
  //        end;
  //      end;
  //    end;
  //
  //  if error = 0 then begin
  //    i := 1;
  //
  //    while (i <= coi) and (error = 0) do begin
  //      error := Container.Array_of_Items[i].Spreadsheet.Load(f);//fix
  //      inc(i);
  //    end;
  //    if error <> 0 then begin
  //      ShowMessage ('Loading error.');
  //      load := error;
  //    end
  //    else load := 0;
  //    close ( f );
  //  end;
  ////end;
end;

  //��������� ���������� � ���������� ����
procedure TMenu.save;
var i:integer = 1;
    f:file;
    error:word = 0;
    doc: TXMLDocument;
    N, sub:TDOMNode;
    E:TDOMElement;
begin

  ///////////////////////////////////////////////

  doc := TXMLDocument.create;
  N := TDOMNode.create(doc);
  sub := TDOMNode.create(doc);

  N := doc.CreateElement('root');
  doc.Appendchild(N);

  ///////////////////////////////////////////////
  if saved.Execute then begin
    Assign(f,saved.FileName);

    Rewrite(f,1);
    blockwrite(f,Container.getCountOfItems,sizeof(longint));

    for i := 1 to Container.getCountOfItems do
      blockwrite(f,Container.Array_of_Items[i].Spreadsheet.GetNumOfRows(),sizeof(Container.Array_of_Items[i].Spreadsheet.GetNumOfRows()));

  ///////////////////////////////////////////////

    i := 1;
    while (i <= Container.getCountOfItems) and (error = 0) do begin
      ///////////////////////////////////////////////

      //��� ������ ������� �������� ���� ��� � XML
       N := doc.DocumentElement;
       sub := doc.CreateElement('Sheet');
       E := sub as Tdomelement;
       E['Name']:='Table'+i.ToString;
       N.AppendChild(sub);

      ///////////////////////////////////////////////
      error := Container.Array_of_Items[i].Spreadsheet.Save(f, doc, N, sub, E);

      inc(i);
    end;
    if error <> 0 then ShowError(error);
     writeXMLFile(doc, 'xml-file.xml');
     doc.free;
    close ( f );
  end;
end;


  //
  procedure TMenu.Redirection ( sender : TObject );
  var i : integer;
  begin
    case ( ( sender as tButton ).Tag ) of
      1:begin
          for i := 1 to 3 do
            if ( arr_Buttons[i].Enabled = FALSE ) then begin
              arr_Buttons[i].Enabled:= TRUE;
              Container.Array_of_Items[i].Spreadsheet.FreeVis;
            end;
          Container.Array_of_Items[( sender as tButton ).Tag].Spreadsheet.display;
          arr_Buttons[( sender as tButton ).Tag].Enabled := FAlSE;
          //container.execute( 1 );
      end;
      2:begin
          for i := 1 to 3 do
            if ( arr_Buttons[i].Enabled = FALSE ) then begin
              arr_Buttons[i].Enabled:= TRUE;
              Container.Array_of_Items[i].Spreadsheet.FreeVis;
            end;
          Container.Array_of_Items[( sender as tButton ).Tag].Spreadsheet.display;
          arr_Buttons[( sender as tButton ).Tag].Enabled := FAlSE;
          //container.execute( 2 );
      end;
      3:begin
          for i := 1 to 3 do
            if ( arr_Buttons[i].Enabled = FALSE ) then begin
             arr_Buttons[i].Enabled:= TRUE;
             Container.Array_of_Items[i].Spreadsheet.FreeVis;
            end;
          Container.Array_of_Items[( sender as tButton ).Tag].Spreadsheet.display;
          arr_Buttons[( sender as tButton ).Tag].Enabled := FAlSE;
          //container.execute( 3 );
      end;
      4:begin

          save;
      end;
      5:begin
          if opend.Execute then begin
            if load(opend.FileName) > 0 then begin
              for i := 1 to 3 do arr_Buttons[i].Enabled := FALSE;
              arr_Buttons[4].Enabled := FALSE;
            end
            else begin
              for i := 1 to 3 do arr_Buttons[i].Enabled := TRUE;
              arr_Buttons[4].Enabled := TRUE;
            end;
          end;
      end;
      6:for i := 1 to 3 do if ( arr_Buttons[i].Enabled = FALSE ) then begin
          Container.Array_of_Items[i].Spreadsheet.freeVis;
          Container.Array_of_Items[i].Spreadsheet.display;
      end;
      7:if opend.execute then begin
          ReadColors( opend.FileName );
      end;
    end;
  end;

  procedure TMenu.ReadColors ( filename : string);
  //type
  var IniFile : TIniFile;
      i       : integer;
      str     : string;
  begin
    IniFile := TIniFile.create ( filename );

    for i := 1 to Container.getCountOfItems() do begin
      //
      str := IniFile.ReadString('spr' + inttostr(i),'alternative color','NotFound');
      if ( str = 'NotFound' ) or ( str = '' ) then begin
        //Container.Array_of_Items[i].Spreadsheet.setAltColor( $00FFFFFF );
        ShowMessage('In section spr' + inttostr(i) + ' key of ident: alternative color not found!' );
      end else Container.Array_of_Items[i].Spreadsheet.setAltColor( strtoint(str) );
      //
      str := IniFile.ReadString('spr' + inttostr(i),'color','NotFound');
      if ( str = 'NotFound' ) or ( str = '' ) then begin
        //Container.Array_of_Items[i].Spreadsheet.setAltColor( $00FFFFFF );
        ShowMessage('In section spr' + inttostr(i) + ' key of ident: color not found!' );
      end else Container.Array_of_Items[i].Spreadsheet.setColor( strtoint(str) );
      //
      str := IniFile.ReadString('spr' + inttostr(i),'fixed color','NotFound');
      if ( str = 'NotFound' ) or ( str = '' ) then begin
        //Container.Array_of_Items[i].Spreadsheet.setAltColor( $00FFFFFF );
        ShowMessage('In section spr' + inttostr(i) + ' key of ident: fixed color not found!' );
      end else Container.Array_of_Items[i].Spreadsheet.setFixColor( strtoint(str) );
      //
      str := IniFile.ReadString('spr' + inttostr(i),'font color','NotFound');
      if ( str = 'NotFound' ) or ( str = '' ) then begin
        //Container.Array_of_Items[i].Spreadsheet.setAltColor( $00FFFFFF );
        ShowMessage('In section spr' + inttostr(i) + ' key of ident: font color not found!' );
      end else Container.Array_of_Items[i].Spreadsheet.setFontColor( strtoint(str) );
    end;
  end;

  //�������� ��������� ����
  procedure TMenu.MenuDisplaying;
  begin
    clrscr;
    MenuGUI.DrawMenu(container);
  end;


end.

