unit Loops;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,crt,types_;
type
         //Мастер класс(абстрактный) зацикливания, от которого наследуются другие классы зацикливания

  TLoopInterface = class
  public
    procedure redirection(k:char);virtual;abstract;
  end;


  //
  TLoop = class
  public
    Item : TLoopInterface;
    procedure Loop;
    constructor create(obj : TLoopInterface);
  end;
implementation

//////    TLoop    //////

constructor TLoop.create (obj : TLoopInterface);
begin
Item := obj;
end;

//Цикл, отвечающий за нажатие клавиш
procedure TLoop.Loop;
var k:char;
begin
Item.redirection(#255);
repeat
k := readkey;
Item.redirection(k);
until (k = #27);
end;


end.

