unit Sheets;

{$mode objfpc}{$H+}
{$define arr}
interface

uses
  Classes, SysUtils, crt, Loops, types_, grids, forms, StdCtrls, controls, DOM, XMLRead, XMLWrite;

type

  arr10int  = array [1..10] of integer;
  arr10bool = array [1..10] of boolean;

  //������ �����(�����������) �������, �� �������� ����������� ������ ������ �������
  TMaster_str = class
  public
    constructor create;virtual;abstract;
    destructor free;virtual;abstract;
    procedure print;virtual;abstract;
    function setValue(s:string):boolean;virtual;abstract;
    function getValue:string;virtual;abstract;
    function compare ():boolean;virtual;//abstract;
    function Save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;virtual;abstract;
    function Load(var doc:TXMLDocument; Child:TDOMNode):word;virtual;abstract;
  end;

  //����� ������� ��� �����
  TStr_str = class(TMaster_str)
  private
    value:string[64];
  public
    constructor create;
    destructor free;
    function compare (second : TStr_str) : boolean;virtual;//override;
    procedure print;override;
    function setValue(s:string):boolean;override;
    function getValue:string;override;
    function Save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;override;
    function Load(var doc:TXMLDocument; Child:TDOMNode):word;override;
  end;

  //����� ������� ��� ����
  TDate_str = class(TMaster_str)
  private
    year:word;//2 bytes
    day,month,hour,minute:byte;//1 byte
  public
    constructor create;
    destructor free;
    function compare (second : TDate_str) : boolean;//override;
    procedure print;override;
    function setValue(s:string):boolean;override;
    function getValue:string;override;
    function Save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;override;
    function Load(var doc:TXMLDocument; Child:TDOMNode):word;override;
  end;

  //����� ������� ��� �����
  TCode_str = class(TMaster_str)
  private
    Code:longint;//4 bytes
  public
    constructor create;
    destructor free;
    function compare (second : TCode_str) : boolean;//override;
    procedure print;override;
    function setValue(s:string):boolean;override;
    function getValue:string;override;
    function Save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;override;
    function Load(var doc:TXMLDocument; Child:TDOMNode):word;override;
  end;

  //����� ������� ��� �������
  TNumber_str = class(TMaster_str)
  private
    Numer:word;//2 bytes
  public
    constructor create;
    destructor free;
    function compare (second : TNumber_str) : boolean;//override;
    procedure print;override;
    function setValue(s:string):boolean;override;
    function getValue:string;override;
    function Save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;override;
    function Load(var doc:TXMLDocument; Child:TDOMNode):word;override;
  end;

  //����� ����(������ �������)
  TField = class
  private
    lenght_of_field : integer;
    value : TMaster_str;
  public
    procedure setValue (s:string);
    constructor create (len : integer;t : types);
    destructor free;
    procedure display (yCoord : integer;columns : integer; bordvis : boolean);virtual;
    function getValue: string;
    function Save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;
    function Load(var doc: TXMLDocument; Child: TDOMNode):word;
  end;

  //����� ������� ���������� ���������������� ������������ � �������
  TCursor = class(TField)
    private
      posX: integer;
      posY: integer;
    public
      procedure setPosX(n : integer);
      procedure setPosY(n : integer);
      function getPosY: integer;
      function getPosX: integer;
      constructor create;
      destructor free;
      procedure display (yCoord : integer;columns : integer; color: boolean);
  end;

  //����� ������(������ �� �����)
  TRow = class
  public
    Array_of_fields : array[1..10] of TField;
  public
    constructor create (topics:strings; top_num:integer;var array_of_types_spr:arr_types_1;flag:boolean);
    destructor free;
    function getRow ( num : integer ) : TRow;
    //function compare_rows(second : TRow):boolean;
    procedure freeRowFields(num:integer);
    procedure display(yCoord:integer; columns: arr_colms; num_of_columns:integer; array_of_types_spr : arr_types_1);
    function getFieldValue(i: integer): string;
    function Save(var f:file;num_of_topics:integer;var doc:TXMLDocument;N, sub:TDOMNode; var E:TDOMElement):word; //num_of_bonded_col : arr10int; arr_of_spr_ptr : arr10Spr; arr_of_bonds_col : arr10bool ):word;
    function Load(var doc: TXMLDocument; Child: TDOMNode;var num_of_topics:integer):word; //num_of_bonded_col : arr10int; arr_of_spr_ptr : arr10Spr; arr_of_bonds_col : arr10bool ):word;
  end;

  pList = ^list;
  List = record
    item : TRow;
    next : pList;
  end;

  //����� �������(������� �� �����)
  TSpreadsheet = class(TLoopInterface)
  private
    SearchSpr          : TSpreadsheet;
    CloseSearchButton  : TButton;
    ChoiceBox          : TCombobox;
    SearchButton       : TButton;
    AddButton          : TButton;
    DelButton          : TButton;
    SortButton         : TButton;
    ConfirmButton      : TButton;
    Search_field       : TEdit;
    Edit_field         : TEdit;
    Form               : TForm;
    Spr                : TStringGrid;
    Spr_of_searching   : boolean;
    current_page       : integer;
    page_number        : integer;
    num_of_topics      : integer;
    //num_of_rows        : integer;
    cursor             : TCursor;
    Topic_row          : TRow;
    Array_of_xCoord    : array [1..10] of integer;

    Alter_color        : longint;
    color              : longint;
    fix_color          : longint;
    font_color         : longint;

    //array_of_types_spr : arr_types_1;

    {$ifdef arr}
    Array_of_rows      : array [1..100] of TRow;
    {$endif}

    {$ifdef din}
    list_of_row      :  List;
    {$endif}

    procedure PageDown;

  public

    num_of_rows        : integer;
    array_of_types_spr : arr_types_1;

    num_of_bonded_col  : array [1..10] of integer;
    arr_of_spr_ptr     : array [1..10] of TSpreadsheet;
    arr_of_bonds_col   : array [1..10] of boolean;

    //function getRow (i : integer ) : TRow;
    //procedure setRow ( i : integer; var buf : TRow );
    //procedure setRow ( i : integer; var buf : TRow );
    procedure SetFixColor ( Col : longint );
    procedure SetColor ( Col : longint );
    procedure SetFontColor ( Col : longint );
    procedure SetAltColor ( Col : longint );

    function  getSpr (): TStringGrid;
    function  getRow (i : integer ) : TRow;
    procedure setRow ( i : integer; var buf : TRow );
    procedure AddRow ( array_of_types : arr_types_1 );
    function SetNumOfRows ( num : integer ) : integer;
    function GetNumOfRows () : integer;
    procedure MouseUp ( sender : TObject; Mouse : TMouseButton; ShState : TShiftState; coord1 : Longint; coord2 : Longint );
    procedure freeVis;
    constructor create( topics : strings; top_num : integer; var arr : arr_types_1; form2 : TForm; flag : boolean );
    destructor free;
    procedure bond_edit ();
    procedure sort;
    procedure find ( num_of_columns:integer; var array_of_types:arr_types_1; SearchObject:string );
    procedure edit ( var s:string);
    procedure display;
    procedure DeleteRow ( num:integer );
    function getXcoord ( num_of_column : integer ) : integer;
    function getColumns  : integer;
    function getRowFieldValue ( x, y : integer ) : string;
    procedure Redirection ( sender : TObject );
    //procedure FormDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

    function Save ( var f : file ;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement) : word;
    function Load ( var doc: TXMLDocument; Child: TDOMNode ) : word;
  end;

  arr10spr  = array [1..10] of TSpreadsheet;

  /////////////////////////////////////////


implementation

//////    TStr_str    //////
//
  constructor TStr_str.create;
  begin
    value := 'Enter string';
  end;

//
 function TStr_str.save(var f:file; var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement ):word;
 var len:word;
     i:integer = 1;
 begin
   len := length(value);

   //���������� ���-�� ��������
   //���������� ������

   N := sub.ParentNode;
   sub := doc.CreateElement('Field');
   E := sub as Tdomelement;
   E['Type']:='String';
   sub.TextContent := value;
   N.AppendChild(sub);

   {$I-}
   blockwrite(f,len,2);
   {$I+}
   save := ioresult;
   //������� ������
   {$I-}
   while (i<=len) and (save = 0) do begin
     blockwrite(f,value[i],1);
     inc(i);
     save := ioresult;
   end;
   {$I+}
 end;

function TStr_str.load(var doc:TXMLDocument; Child:TDOMNode):word;
 var len:word;
     i:integer = 1;
     c:char;
     s:string;
 begin
   s:= child.NodeName;
   value := Child.TextContent;

   //{$I-}
   //blockread(f,len,2);
   //{$I+}
   //load := ioresult;
   ////������� ������
   //{$I-}
   //value := '';
   //while (i<=len) and (load = 0) do begin
   //  {$I-}
   //  blockread(f,c,1);
   //  {$I+}
   //  value := value+c;
   //  inc(i);
   //  load := ioresult;
   //end;
 end;

//
  destructor TStr_str.free;
  begin

  end;

//
  function TStr_str.compare ( second : TStr_str ) : boolean;
  begin
    if (value > second.value) then compare := true;
  end;

//
  function TStr_str.getValue () : string;
  begin
    getValue:=value;
  end;

//��������, ������������� ���, ���������� ����� �� ������ ��������
  function TStr_str.setValue(s:string):boolean;
  begin
    value := s;
    setValue:=true;
  end;

//������� ���
  procedure TStr_str.print;
  begin
    if (length(value) < 14) then write(value)
    else write(copy(value,1,14));
  end;

//////    TDate_str    //////
//
  constructor TDate_str.create;
  begin
    day:=0;
    month:=0;
    year:=0;
    hour:=0;
    minute:=0;
  end;

//
  function TDate_str.save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;
  begin
    //���������� ���� ��� 3 ����
    N := sub.ParentNode;
    sub := doc.CreateElement('Field');
    E := sub as Tdomelement;
    E['Type']:='Date';
    //sub.TextContent := Year.ToString+'.'+month.ToString+'.'+day.ToString+' '+hour.ToString+':'+minute.ToString;
    N.AppendChild(sub);

    N := sub;
    sub := doc.CreateElement('Year');
    sub.TextContent := year.ToString;
    N.AppendChild(sub);

    N := sub.ParentNode;
    sub := doc.CreateElement('Month');
    sub.TextContent := month.ToString;
    N.AppendChild(sub);

    N := sub.ParentNode;
    sub := doc.CreateElement('Day');
    sub.TextContent := day.ToString;
    N.AppendChild(sub);

    N := sub.ParentNode;
    sub := doc.CreateElement('Hour');
    sub.TextContent := hour.ToString;
    N.AppendChild(sub);

    N := sub.ParentNode;
    sub := doc.CreateElement('Minute');
    sub.TextContent := minute.ToString;
    N.AppendChild(sub);
    sub := N.ParentNode;

    {$I-}
    blockwrite(f,day,1);
    {$I+}
    save := ioresult;
    if save = 0 then begin
      {$I-}
      blockwrite(f,month,1);
      {$I+}
      save := ioresult;
    end;
    if save = 0 then begin
      {$I-}
      blockwrite(f,year,2);
      {$I+}
      save := ioresult;
    end;
    if save = 0 then begin
      {$I-}
      blockwrite(f,hour,1);
      {$I+}
      save := ioresult;
    end;
    if save = 0 then begin
      {$I-}
      blockwrite(f,minute,1);
      {$I+}
      save := ioresult;
    end;
  end;

function TDate_str.load(var doc:TXMLDocument; Child:TDOMNode):word;
  begin

    year := strtoint(Child.FirstChild.TextContent);
    month := strtoint(Child.FirstChild.NextSibling.TextContent);
    day := strtoint(Child.FirstChild.NextSibling.NextSibling.TextContent);
    hour := strtoint(Child.FirstChild.NextSibling.NextSibling.NextSibling.TextContent);
    minute := strtoint(Child.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.TextContent);

    //{$I-}
    //blockread(f,day,1);
    //{$I+}
    //load := ioresult;
    //if load = 0 then begin
    //  {$I-}
    //  blockread(f,month,1);
    //  {$I+}
    //  load := ioresult;
    //end;
    //if load = 0 then begin
    //  {$I-}
    //  blockread(f,year,2);
    //  {$I+}
    //  load := ioresult;
    //end;
    //if load = 0 then begin
    //  {$I-}
    //  blockread(f,hour,1);
    //  {$I+}
    //  load := ioresult;
    //end;
    //if load = 0 then begin
    //  {$I-}
    //  blockread(f,minute,1);
    //  {$I+}
    //  load := ioresult;
    //end;
  end;

//
  destructor TDate_str.free;
  begin

  end;
//
  function TDate_str.compare ( second : TDate_str) : boolean;
  begin
    if year > second.year then compare := true
    else if year < second.year then compare := false
      else
    //
      if month > second.month then compare := true
      else if month < second.month then compare := false
        else
    //
        if day > second.day then compare := true
        else if day < second.day then compare := false
          else
    //
          if hour > second.hour then compare := true
          else if hour < second.hour then compare := false
            else
    //
            if minute > second.minute then compare := true
            else compare := false;
  end;

//��������, ������������� ����, ���������� ����� �� ������ ��������
  function TDate_str.setValue(s:string):boolean;
  var e,d,m,y,h,min,buf:integer;
      vis:boolean;
  begin
    if (s [5] = '.') and (s [8] = '.') {and (s [11] = ' ') and (s [14] = ':')} then begin
      val (copy(s,1,4),y,e);
      if (y < 1990) or (y > 2100) then setValue:= false
      else begin
        if ((y mod 4 = 0) and (y mod 100 <> 0)) or (y mod 400 = 0) then vis := true
        else vis := false;
        val (copy(s,6,2),m,e);
        if (m > 12) or (m < 0) then setValue:= false
        else begin
          case (m) of
           2: if (vis = true) then buf :=29
              else buf := 28;
           1,3,5,7,8,10,12: buf := 31;
           4,6,9,11: buf := 30;
          end;
          val (copy(s,9,2),d,e);
          if (d < 0) or (d > buf) then setValue:= false
          else begin
            {val (copy(s,12,2),h,e);
            if (h > 23) or (h < 0) then setValue:= false
            else begin
              val (copy(s,15,2),min,e);
              if (min > 60) or (min < 0) then setValue:= false
              else begin }
                day:=d;
                month:=m;
                year:=y;
                //hour:=h;
                //minute:=min;
           //   end;
          //  end;
          end;
        end;
      end;
    end;
  end;

//������� ����
  procedure TDate_str.print;
  begin
    write(Year mod 100,'.',month:2,'.',day:2,' ',hour:2,':',minute:2);
  end;

function TDate_str.getValue():string;
begin
  getValue := '';
  getValue:= getValue + char(year div 1000 mod 10 + 48) + char(year div 100 mod 10 + 48) + char(year div 10 mod 10 + 48) + char(year mod 10 + 48);
  getValue:= getValue + '.';
  getValue:= getValue + char(month div 10 + 48) + char(month mod 10 + 48);
  getValue:= getValue + '.';
  getValue:= getValue + char(day div 10 + 48) + char(day mod 10 + 48);
  //getValue:= getValue + ' ';
  //getValue:= getValue + char(hour div 10 + 48) + char(hour mod 10 + 48);
  //getValue:= getValue + ':';
  //getValue:= getValue + char(minute div 10 + 48) + char(minute mod 10 + 48);
end;

//////    TCode_str    //////
//
  constructor TCode_str.create;
  begin
    code:=0;
  end;

  function TCode_str.save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;
  begin
    //���������� ������ ��� �����
    N := sub.ParentNode;
    sub := doc.CreateElement('Field');
    E := sub as Tdomelement;
    E['Type']:='Code';
    sub.TextContent := Code.ToString;
    N.AppendChild(sub);
    {$I-}
    blockwrite(f,Code,sizeof(Code));
    {$I+}
    save := ioresult;
  end;

  function TCode_str.load(var doc:TXMLDocument; Child:TDOMNode):word;
  begin

    Code := strtoint(Child.TextContent);
    //{$I-}
    //blockread(f,Code,sizeof(longint));
    //{$I+}
    //load := ioresult;
  end;

//
  destructor TCode_str.free;
  begin

  end;

//
  function TCode_str.compare ( second : TCode_str) : boolean;
  begin
    if ( code > second.code ) then compare := true
    else compare := false;
  end;

//
  function TCode_str.getValue:string;
  var i:integer;
  begin
    getvalue := '';
    i:=1;
    getvalue := inttostr ( code );

    if Code = 0 then getValue := '0';
  end;

//��������, ������������� ���, ���������� ����� �� ������ ��������
  function TCode_str.setValue(s:string):boolean;
  var error:word;
  begin
    if (length(s) <= 4) then val(s,code,error) else error := 1;
    if error = 0 then setValue := true else setValue := false;
  end;

//������� ���
  procedure TCode_str.print;
  begin
    write(code);
  end;

//////    TNumber_str    //////
//
  constructor TNumber_str.create;
  begin
    Numer:=0;
  end;
//

function TNumber_str.save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;
begin
  //���������� ������ ��� �����
   N := sub.ParentNode;
   sub := doc.CreateElement('Field');
   E := sub as Tdomelement;
   E['Type']:='Number';
   sub.TextContent := Numer.ToString;
   N.AppendChild(sub);
  {$I-}
  blockwrite(f,Numer,2);
  {$I+}
  save := ioresult;
end;

function TNumber_str.load(var doc:TXMLDocument; Child:TDOMNode):word;
begin

  Numer := strtoint(Child.TextContent);
  //{$I-}
  //blockread(f,Numer,2);
  //{$I+}
  //load := ioresult;
end;


//
  destructor TNumber_str.free;
  begin

  end;

//
  function TNumber_str.compare ( second : TNumber_Str ) : boolean;
  begin
    if numer > second.numer then compare := true
    else compare := false;
  end;

//
  function TNumber_str.getValue():string;
  var i:integer;
  begin
    getvalue := '';
    getvalue := inttostr (numer) ;

    if numer = 0 then getValue := '0';
  end;

//��������, ������������� �����, ���������� ����� �� ������ ��������
  function TNumber_str.setValue(s:string):boolean;
  var error:word;
  begin

    if length(s) < 4 then val(s,Numer,error) else error := 1;
    if error = 0 then setValue := true else setValue := false;
  end;

//������� �����
procedure TNumber_str.print;
begin
  write(Numer);
end;

//////    TField    //////

constructor TField.create (len : integer;t : types);
begin
  case t of
  date:value := TDate_str.create;
  Str:value := TStr_str.create;
  Code:value := TCode_str.create;
  Number:value := TNumber_str.create;
  end;
  lenght_of_field := len;
end;

destructor TField.free;
begin

end;
//��������, ������������� �������� ������
procedure TField.setValue (s:string);
var flag:boolean;
begin
  flag:=value.setValue(s);// ���� ���, �� ���������� ��������
end;

//������� ������
procedure TField.display (yCoord: integer; columns:integer; bordvis:boolean);
begin
  if ( bordvis = true ) then begin
    gotoxy(columns,yCoord);
    write ('#--------------#');
    gotoxy(columns,yCoord+1);
    write ('|              |');
    gotoxy(columns,yCoord+2);
    write ('#--------------#');
  end;
  gotoxy(columns + 2,yCoord + 1);
  write ('             ');
  gotoxy(columns + 1,yCoord + 1);
  value.print;
end;

//��������, ���������� �������� ������
function TField.getValue: string;
begin
  getValue := value.getValue;
end;

function TField.Save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;
begin
  //�� ��� ������ ���
  save := value.Save(f,doc,N,sub,E);
end;

function TField.Load(var doc: TXMLDocument; Child: TDOMNode):word;
begin
  {$I-}
  load := value.Load(doc,Child);
  {$I+}
end;

//////    TCursor    //////
//
constructor TCursor.create;
begin
  lenght_of_field:=16;
  posX := 1;
  posY := 2;
  value := TStr_str.create;
  value.setValue ( '' );
end;

//
destructor TCursor.free;
begin

end;

//��������, ���������� ������� �� �
function TCursor.getPosX: integer;
begin
  getPosX := posX;
end;

//��������, ���������� ������� �� Y
function TCursor.getPosY: integer;
begin
  getPosY := posY;
end;

//��������, ������������� ������� �� Y
procedure TCursor.setPosY(n : integer);
begin
  posY:= n;
end;

//��������, ������������� ������� �� �
procedure TCursor.setPosX(n : integer);
begin
  posX:= n;
end;

//������� ������
procedure TCursor.display(yCoord : integer;columns : integer; color: boolean);
begin
  if color then TextColor(Green)
  else TextColor(White);
  gotoxy((-1+posX+columns)*16+1,(-1+posY+yCoord)*3+1);
  write ('#--------------#');
  gotoxy((-1+posX+columns)*16+1,(-1+posY+yCoord)*3+2);
  write ('|');
  gotoxy((-1+posX+columns)*16+16,(-1+posY+yCoord)*3+2);
  write ('|');
  gotoxy((-1+posX+columns)*16+1,(-1+posY+yCoord)*3+3);
  write ('#--------------#');
  //inherited display((-1+posY+yCoord)*3+1, (-1+posX+columns)*16+1);
  TextColor(White);
  inc(PosY,yCoord);
  inc(PosX,columns);
end;

//////    TRow    //////
function TRow.getRow ( num : integer ) : TRow;
begin

end;

//
constructor TRow.create (topics:strings; top_num:integer;var array_of_types_spr:arr_types_1;flag:boolean);
var i:integer;
begin
  for i:=1 to top_num do begin
    Array_of_fields[i]:= TField.create(16,array_of_types_spr[i]);
    if flag then Array_of_fields[i].setValue(topics[i]);
  end;
end;
//

function TRow.save(var f:file; num_of_topics:integer;var doc:TXMLDocument; N, sub:TDOMNode; var E:TDOMElement):word; //num_of_bonded_col : arr10int; arr_of_spr_ptr : arr10Spr; arr_of_bonds_col : arr10bool ):word;
var i:integer = 1;
    j:integer;
begin
  save := 0;
  {$I-}
  blockwrite(f,num_of_topics,sizeof(num_of_topics));//���-�� �����

  N := sub.ParentNode;
  sub := doc.CreateElement('Row');
  N.AppendChild(sub);

  N := sub;
  sub := doc.CreateElement('FieldsCount');
  sub.TextContent := num_of_topics.ToString;
  N.AppendChild(sub);



  while (i <= num_of_topics) and (save = 0) do begin
    //if arr_of_bonds_col[i] then
    //  for j := 1 to arr_of_spr_ptr[i].num_of_rows do

    save := Array_of_fields[i].save(f,doc,N,sub,E);
    inc(i);
  end;
  {$I+}
end;

function TRow.load(var doc: TXMLDocument; Child: TDOMNode;var num_of_topics:integer):word;//num_of_bonded_col : arr10int; arr_of_spr_ptr : arr10Spr; arr_of_bonds_col : arr10bool ):word;
var i:integer = 1;
    s:string;
begin
  s := Child.FirstChild.NextSibling.FirstChild.NodeName;
  num_of_topics := strtoint(Child.FirstChild.TextContent);
  child := child.FirstChild;
  s := child.NodeName;
  for i:=1 to num_of_topics do begin

    child := child.NextSibling;
    s := child.NodeName;
    Array_of_fields[i].Load(doc, Child);

  end;

  //load := 0;
  //{$I-}
  //blockread(f,num_of_topics,sizeof(num_of_topics));//���-�� �����
  //i := 1;
  //{$I+}
  //load := ioresult;
  //if ( load = 0 ) and ((num_of_topics <= 0 ) or ( num_of_topics > 7)) then inc ( load );
  //while ( i <= num_of_topics ) and (load = 0) do begin
  //  {$I-}
  //  load := Array_of_fields[i].load(f);
  //  {$I+}
  //  inc(i);
  //end;
end;

//
destructor TRow.free;
begin

end;

//
procedure TRow.freeRowFields(num:integer);
var i:integer;
begin
  for i:=1 to num do begin
    Array_of_fields[i].free;
    Array_of_fields[i] := NIL;
  end;
end;

//����� ������
procedure TRow.display (yCoord:integer; columns: arr_colms; num_of_columns:integer; array_of_types_spr : arr_types_1);
var i:integer;
begin
  for i:=1 to num_of_columns do begin
    if ( Array_of_fields[i] = NIL ) then begin
      Array_of_fields[i] := Array_of_fields[i].create(16,array_of_types_spr[i]);
      Array_of_fields[i].setValue('�������� ���� �������.');
    end;
    Array_of_fields[i].display(yCoord,(-1+i)*16+1, true);
  end;
end;

//��������, ���������� �������� ������ �� �� ������
function TRow.getFieldValue(i: integer): string;
begin
  getFieldValue := Array_of_fields[i].getValue;
end;

//////    TSpreadsheet    //////

procedure TSpreadsheet.MouseUp ( sender : TObject; Mouse : TMouseButton; ShState : TShiftState; coord1 : Longint; coord2 : Longint );
var i : integer;
Begin
//if ( Spr.Row >= 1 ) then //begin
    if arr_of_bonds_col[Spr.Col + 1] and ( ChoiceBox = NIL ) then begin;
      ChoiceBox := TComboBox.Create ( form );

      for i := 1 to arr_of_spr_ptr[Spr.Col + 1].num_of_rows do
        ChoiceBox.AddItem( arr_of_spr_ptr[Spr.Col + 1].getRowFieldValue( num_of_bonded_col[ Spr.Col + 1], i ), NIL );

      ChoiceBox.Top := Edit_field.Top;
      ChoiceBox.Left := Edit_field.Left;
      ChoiceBox.Height := Edit_field.Height;
      ChoiceBox.Width := Edit_field.Width;
      Edit_field.Visible := FALSE;
      ChoiceBox.Parent := form;
      choicebox.ReadOnly := true;
      SortButton.Enabled := TRUE;
      ConfirmButton.Enabled := TRUE;
      Edit_field.Enabled := TRUE;
      ChoiceBox.ItemIndex := 0;
    end
    else begin
      if ( ChoiceBox <> NIL ) and not ( arr_of_bonds_col[Spr.Col + 1]) then begin                                                             //Load
        ChoiceBox.destroy;
        ChoiceBox := NIL;
        //SortButton.Enabled := FALSE;
        //ConfirmButton.Enabled := FALSE;
        Edit_field.Visible := TRUE;
        //if arr_of_bonds_col[Spr.Col + 1] then
        //  Edit_field.Enabled := FALSE;
      end
      else begin
        SortButton.Enabled := TRUE;
        ConfirmButton.Enabled := TRUE;
        Edit_field.Enabled := TRUE;
        if ( ChoiceBox <> NIL ) then
          ChoiceBox.Visible := TRUE;
        //Edit_field.Visible := TRUE;//
      end;

    //  Edit_field.Enabled := TRUE;
      if not ( arr_of_bonds_col [ spr.col + 1 ])  then
        Edit_field.Text := getRowFieldValue ( Spr.Col + 1, Spr.Row )
      else
        Edit_field.Text := ' ';
  //  end;
  end;
end;

//
procedure TSpreadsheet.DragDrop(Sender, Source: TObject; X, Y: Integer);
var i,buf2 : integer;
    ax : integer = 0;
    ay : integer = 0;
    buf : TRow;
begin
  if Source = sender then begin
    Spr.MouseToCell(X,Y,ay,ax);
    buf := Array_of_rows[spr.row];
    if ( spr.row > ax ) then
      for i := spr.row downto ax do Array_of_rows[i] := Array_of_rows[i - 1]
    else
      for i := spr.row to ax do Array_of_rows[i] := Array_of_rows[i + 1];
    //Array_of_rows[spr.row] := Array_of_rows[ax];
    Array_of_rows[ax] := buf;
    Spr.MoveColRow(false,Spr.row,ax);
  end;
end;
//

//
procedure TSpreadsheet.DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=true;
end;
//

//
procedure TSpreadsheet.MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
    Spr.BeginDrag(False, 4);
end;
//

//
constructor TSpreadsheet.create ( topics : strings; top_num : integer; var arr : arr_types_1; form2 : TForm; flag : boolean );
var i  : integer;
  buf  : strings;
  buf2 : arr_types_1;
  buf3 : TRow;
begin
  SearchButton := NIL;
  Spr := NIL;
  Edit_field := NIL;
  Search_field := NIL;
  ConfirmButton := NIL;
  SearchButton := NIL;
  AddButton := NIL;
  DelButton := NIL;
  CloseSearchButton := NIL;

  color := $FFFFFF;
  Alter_color := $FFFFFF;
  fix_color := $E0E0E0;
  font_color := $000000;

  Spr_of_searching := flag;
  Form := form2;

  for i:=1 to top_num do begin
    buf2[i] := Str;
    arr_of_spr_ptr[i] := NIL;
  end;

  array_of_types_spr := arr;
  current_page := 1;
  page_number := 1;
  num_of_topics:= top_num;
  Cursor := TCursor.create;
  if (top_num < 11) then begin
    Topic_row := TRow.create(topics,top_num,buf2,True);
    num_of_rows := 1;//0
    for i:=1 to 10 do buf[i] := '';
    buf3 := TRow.create( buf, top_num, array_of_types_spr, False );
    setRow( 1, buf3);
  end;
end;

//
function TSpreadsheet.getRow ( i : integer ) : TRow;
var
  ptr : pList;
  j   : integer;
begin
  {$ifdef arr}

  getRow := Array_of_rows [i];

  {$endif}

  {$ifdef din}

   ptr := @list_of_row;
   for i := 2 to i do
     ptr := ptr^.next;
   getRow := ptr^.item;

  {$endif }
end;

procedure TSpreadsheet.freeVis;
begin
  if ( Spr <> NIL ) then Spr.Destroy;
  if ( Edit_field <> NIL ) then Edit_field.Destroy;
  if ( Search_field <> NIL ) then Search_field.Destroy;
  if ( ConfirmButton <> NIL ) then ConfirmButton.Destroy;
  if ( SearchButton <> NIL ) then SearchButton.Destroy;
  if ( AddButton <> NIL ) then AddButton.Destroy;
  if ( DelButton <> NIL ) then DelButton.Destroy;
  if ( CloseSearchButton <> NIL ) then CloseSearchButton.Destroy;
  if ( ChoiceBox <> NIL ) then begin
    ChoiceBox.Destroy;
    ChoiceBox := NIL;
  end;
end;

//
procedure TSpreadsheet.setRow ( i : integer; var buf : TRow );
var
  ptr : pList;
  j   : integer;
begin
  {$ifdef arr}

  Array_of_rows [i] := buf;

  {$endif}

  {$ifdef din}

   ptr := @list_of_row;
   for i := 2 to i do
     ptr := ptr^.next;
   ptr^.item := buf;

  {$endif }
end;

//
function TSpreadsheet.Save(var f:file;var doc:TXMLDocument; var N, sub:TDOMNode; var E:TDOMElement):word;
var i : integer = 1;
    j : integer;
    k : integer;
    pos : integer;
begin
  //{$I-}
  //blockwrite(f,num_of_rows,sizeof(num_of_rows));//���-�� �����
  //{$I+}
      N := sub;
    sub := doc.CreateElement('RowsCount');
    sub.TextContent := num_of_rows.ToString;
    N.AppendChild(sub);
  save := ioresult;
  while (i <= num_of_rows) and (save = 0) do begin
    ////////////////////////////////////////////////////////////////

    //doc := TXMLDocument.create;      //�������� � ������ ����

    //���������� ���� ��� Fields

    ////////////////////////////////////////////////////////////////
    save := getRow(i).Save(f,num_of_topics,doc,N,sub,E);

    inc(i);
  end;
  for i := 1 to num_of_topics do
    if arr_of_bonds_col[i] then
      for j := 1 to num_of_rows do begin
        pos := 0;
        k := 1;
        while ( k <= arr_of_spr_ptr[i].num_of_rows ) and ( pos = 0 ) do begin
          if Array_of_rows[j].Array_of_fields[i] = arr_of_spr_ptr[i].Array_of_rows[k].Array_of_fields[num_of_bonded_col[i]] then
            pos := k;
          inc (k);
        end;
        {$I-}
        blockwrite(f,pos,sizeof(pos));
        {$I+}
      end;
  save := ioresult;
end;

function TSpreadsheet.Load(var doc: TXMLDocument; Child: TDOMNode):word;
var  i   : integer = 1;
     j   : integer;
   buf   : TRow;
   str   : strings;
   buf2  : integer;
begin
  //{$I-}
  //blockread(f,num_of_rows,sizeof(longint));//���-�� �����
  //{$I+}

  num_of_rows := strtoint(Child.FirstChild.FirstChild.TextContent);

  for i:=1 to num_of_rows do begin

    Array_of_rows[i].Load(doc, Child.FirstChild.NextSibling, num_of_topics);
    child:=child.NextSibling;
  end;

  ////load := ioresult;
  //load := 0;
  //if (( num_of_rows > 100 ) or ( num_of_rows < 0 )) and ( load = 0 ) then inc ( load );
  //while (i <= num_of_rows) and (load = 0) do begin
  //  {$I-}
  //  load := getRow(i).load(f,num_of_topics);
  //  {$I+}
  //  if load = 0 then load := ioresult;
  //  inc(i);
  //end;
  //for i := 1 to num_of_rows do
  //  for j := 1 to num_of_topics do
  //    if arr_of_bonds_col[j] then begin
  //      {$I-}
  //      blockread(f,buf2,sizeof(buf2));
  //      {$I+}
  //      if buf2 = 0 then Array_of_rows[i].Array_of_fields[j] := NIL
  //      else Array_of_rows[i].Array_of_fields[j] := arr_of_spr_ptr[j].Array_of_rows[buf2].Array_of_fields[num_of_bonded_col[j]];
  //    end;
end;

//
destructor TSpreadsheet.free;
begin

end;

//����� �������
procedure TSpreadsheet.display ;
var i, j, z, buf, buf2 : integer;
    flag               : boolean;
begin
  //Topic_row.display(1, Array_of_xCoord, num_of_topics,array_of_types_spr);
  Spr := TStringGrid.Create ( form );
  Spr.Color := color;
  Spr.AlternateColor := Alter_color;
  spr.FixedColor := fix_color;
  spr.Font.Color := font_color;
  Spr.Top := 50;
  Spr.Left := 116;
  Spr.Width := 554;
  Spr.Height := 254;
  Spr.FixedCols := 0;
  Spr.ColCount := num_of_topics;
  Spr.RowCount := num_of_rows + 1;
  Spr.OnMouseUp := @MouseUp;
  Spr.OnMouseDown := @MouseDown;
  Spr.OnDragOver := @DragOver;
  Spr.OnDragDrop := @DragDrop;

  for i := 0 to num_of_topics - 1 do
    Spr.ColWidths[i] := length ( Topic_row.getFieldValue( i + 1 ) ) * 5;

  for i := 0 to num_of_topics - 1 do
    Spr.Cells[i, 0] := Topic_row.getFieldValue( i + 1 );

  for i := 1 to num_of_topics do
    if ( arr_of_bonds_col[i] = true ) then
      for j := 1 to num_of_rows do begin
        flag := false;
        for z := 1 to arr_of_spr_ptr[i].num_of_rows do begin
            if ( getRow(j).Array_of_fields[i] = arr_of_spr_ptr[i].getRow( z ).Array_of_fields[num_of_bonded_col[i]] ) then flag := true;
          if ( flag <> true ) and ( z = arr_of_spr_ptr[i].num_of_rows ) then begin
            getRow(j).Array_of_fields[i] := Tfield.create(16,array_of_types_spr[i]);
            getRow(j).Array_of_fields[i].setValue('Communications not detected');
          end;
        end;
      end;

  for i := 1 to num_of_rows do
    for j := 0 to num_of_topics - 1 do begin
      Spr.Cells[j, i] := getRowFieldValue( j + 1, i );
    end;

  Spr.Parent := form;

  if Spr_of_searching then flag := false
  else flag := true;

  Edit_field := TEdit.Create ( form );
  Edit_field.Top := 16;
  Edit_field.Left := 116;
  Edit_field.Width := 454;
  Edit_field.Height := 23;
  Edit_field.Enabled := FALSE;
  Edit_field.Parent := form;

  ConfirmButton := TButton.Create ( form );
  ConfirmButton.Top := 16;
  ConfirmButton.Left := 600;
  ConfirmButton.Width := 70;
  ConfirmButton.Height := 23;
  ConfirmButton.Caption := 'Confirm';
  ConfirmButton.Enabled := FALSE;
  ConfirmButton.OnClick := @Redirection;
  ConfirmButton.Tag := 1;
  ConfirmButton.Parent := form;

  SortButton := TButton.Create ( form );
  SortButton.Top := 320;
  SortButton.Left := 264;
  SortButton.Width := 70;
  SortButton.Height := 25;
  SortButton.Caption := 'Sort';
  SortButton.Enabled := FALSE;
  SortButton.OnClick := @Redirection;
  SortButton.Tag := 6;
  SortButton.Parent := form;

  Search_field := TEdit.Create ( form );
  Search_field.Top := 320;
  Search_field.Left := 376;
  Search_field.Width := 216;
  Search_field.Height := 23;
  Search_field.Parent := form;
  Search_field.Enabled := flag;

  SearchButton := TButton.Create ( form );
  SearchButton.Top := 320;
  SearchButton.Left := 609;
  SearchButton.Width := 61;
  SearchButton.Height := 25;
  SearchButton.Caption := 'Search';
  SearchButton.Enabled := flag;
  SearchButton.OnClick := @Redirection;
  SearchButton.Tag := 2;
  SearchButton.Parent := form;

  DelButton := TButton.Create ( form );
  DelButton.Top := 320;
  DelButton.Left := 190;
  DelButton.Width := 70;
  DelButton.Height := 25;
  DelButton.Caption := 'Delete row';
  DelButton.Enabled := flag;
  DelButton.OnClick := @Redirection;
  DelButton.Tag := 3;
  DelButton.Parent := form;

  AddButton := TButton.Create ( form );
  AddButton.Top := 320;
  AddButton.Left := 116;
  AddButton.Width := 70;
  AddButton.Height := 25;
  AddButton.Caption := 'Add row';
  AddButton.Enabled := TRUE;
  AddButton.OnClick := @Redirection;
  AddButton.Tag := 4;
  AddButton.Parent := form;
  AddButton.Enabled := flag;

  if ( not ( ChoiceBox = NIL ) ) and ( arr_of_bonds_col[Spr.Col] ) then ChoiceBox.Visible := true;

  if Spr_of_searching then  begin
    CloseSearchButton := TButton.Create ( form );
    CloseSearchButton.Top := 320;
    CloseSearchButton.Left := 609;
    CloseSearchButton.Width := 61;
    CloseSearchButton.Height := 25;
    CloseSearchButton.Caption := 'Close';
    CloseSearchButton.Enabled := TRUE;
    CloseSearchButton.OnClick := @Redirection;
    CloseSearchButton.Tag := 5;
    CloseSearchButton.Parent := form;
  end;

  {if current_page < page_number then buf:= 8
  else buf := num_of_rows mod 8;
  if buf = 0 then buf := 8 ;
  buf2:=(current_page - 1) * 8;
  }
  {for i := 1 to buf do begin
    getRow(buf2 + i).display(i*3+1,Array_of_xCoord,num_of_topics,array_of_types_spr);
  end;}
end;

//����� ��� ������
procedure TSpreadsheet.Redirection ( sender : TObject );
var SearchObject:string;
    i:integer;
    bufs: strings;
    s:string;
begin
  case ( sender as TButton ).Tag of
    //confirm button
    1:begin
      if arr_of_bonds_col[Spr.Col + 1] then
        getRow( Spr.Row ).Array_of_fields[Spr.Col + 1] := arr_of_spr_ptr[Spr.Col + 1].getRow( ChoiceBox.ItemIndex + 1 ).Array_of_fields[num_of_bonded_col[spr.col + 1]]
      else begin
        getRow( Spr.Row ).Array_of_fields[Spr.Col + 1].setValue( Edit_field.Text );
        Edit_field.Text := ' ';
      end;
      Spr.Cells[Spr.Col, Spr.Row] := getRowFieldValue( Spr.Col + 1, Spr.Row );
  end;
    2:begin
        if ( SearchSpr <> NIL ) then begin
          SearchSpr.freeVis;
          SearchSpr.destroy;
        end;
        s := Search_field.Text;
        find ( num_of_topics, array_of_types_spr, s );
        SearchSpr.display;
    end;
    3:if ( num_of_rows > 1 ) then begin
        DeleteRow( Spr.Row );
        Spr.DeleteRow( Spr.Row );
    end;
    4:if ( num_of_rows < 100) then begin
        Spr.RowCount := Spr.RowCount + 1;
        AddRow( array_of_types_spr );
        for i := 0 to num_of_topics - 1 do
          Spr.Cells[i,num_of_rows] := getRowFieldValue( i + 1, num_of_rows );
    end;
    5:begin
        CloseSearchButton.Visible := FALSE;
        SearchButton.Visible := FALSE;
        AddButton.Visible := FALSE;
        DelButton.Visible := FALSE;
        Spr.Visible := FALSE;
        Edit_field.Visible := FALSE;
        ConfirmButton.Visible := FALSE;
        Search_field.Visible := FALSE;
    end;
    6:begin
        sort;
        freevis;
        display;
    end;
  end;
end;

//editor
procedure TSpreadsheet.bond_edit ;
var
  key : char;
  s : string;
  i : integer = 1;
begin
  while ( i < arr_of_spr_ptr [cursor.getPosX].num_of_rows + 1) and ( getRow( cursor.getPosY - 1 ).Array_of_fields[cursor.getPosX].getValue <> arr_of_spr_ptr [cursor.getPosX].getRow( i ).Array_of_fields[num_of_bonded_col[cursor.getPosX]].getValue ) do begin
    inc(i);
  end;
  if ( arr_of_spr_ptr [cursor.getPosX].num_of_rows < i ) then i := 1;
  s := arr_of_spr_ptr [cursor.getPosX].getRow( i ).Array_of_fields[num_of_bonded_col[cursor.getPosX]].getValue;
  gotoxy(2,30);
  write('                                                                ');
  gotoxy(2,30);
  write(s);
  while (key <> #13) and (key <> #27) do begin
    key:=readkey;
    if ( key = #0 ) then key:=readkey;
    if (key = #72) and ( i > 1 ) then begin
      dec(i);
    end;
    if (key = #80) and ( i < arr_of_spr_ptr [cursor.getPosX].num_of_rows ) then begin
      inc(i);
    end;
    s := arr_of_spr_ptr [cursor.getPosX].getRow( i ).Array_of_fields[num_of_bonded_col[cursor.getPosX]].getValue;
    gotoxy(2,30);
    write('                                                                ');
    gotoxy(2,30);
    write(s);
  end;
  getRow( Cursor.getPosY - 1 ).Array_of_fields[ Cursor.getPosX ] := arr_of_spr_ptr [cursor.getPosX].getRow( i ).Array_of_fields[num_of_bonded_col[cursor.getPosX]];
  gotoxy( 2, 30 );
  write('                                                                ');
end;

procedure TSpreadsheet.edit(var s:string);
var key : char;
begin
  s:= getRow ( cursor.getPosY - 1 ).Array_of_fields[cursor.getPosX].getValue;
  gotoxy(length(s)+1,30);
  while (key <> #13) and (key <> #27) do begin
    key:=readkey;
    if (key<>#0#80)then
      if (length(s) < 64) and (key in ['a'..'z','A'..'Z',' ','.',',','0'..'9',':']) then s := s+key;
    if (key = #0) then key := readkey;
    if (key = #8) and (length(s) > 0) then begin
      s:=copy(s,1,length(s) - 1);
      gotoxy(2,30);
      write('                                                                ');
    end;
    gotoxy(2,30);
    write(s);
  end;
  gotoxy(2,30);
  write('                                                                ');
end;

procedure TSpreadsheet.find ( num_of_columns : integer; var array_of_types : arr_types_1; SearchObject : string );
var
    count : integer = 0;
    Loop  : TLoop;
    i,j   : integer;
    flag  : boolean;
    buf2  : TRow;
    tops  : strings;
begin
  for i := 1 to num_of_topics do
    tops[i] := Topic_row.Array_of_fields[i].getValue;

  SearchSpr := TSpreadsheet.create ( tops,num_of_columns,array_of_types, form, TRUE );

  for i := 1 to num_of_rows do begin
    j := 1;
    flag := false;
    while ( j <= num_of_topics ) and not ( flag ) do begin
      if getRow( i ).Array_of_fields[j].getValue = SearchObject then begin
        inc ( count );
        buf2 := getRow ( i );
        SearchSpr.setRow( count, buf2 );
        flag := true;
      end;
      inc ( j );
    end;
  end;
  SearchSpr.num_of_rows := count;

end;

//
procedure TSpreadsheet.sort;
var i,j : integer;
    buf, buf2 : TRow;
begin

  for i := num_of_rows downto 1 do
    for j := 1 to i - 1 do
      if ( getRow( j ).Array_of_fields[Spr.Col + 1].getValue < getRow( j + 1 ).Array_of_fields[Spr.Col + 1].getValue = false ) then begin
        buf := getRow( j );
        buf2 := getRow( j + 1 );
        setRow( j + 1, buf );
        setRow( j, buf2 );
      end;
end;

//��������, ���������� ���������� �� �
function TSpreadsheet.getXcoord (num_of_column:integer):integer;
begin
  getXcoord := num_of_topics * 9;
end;

//�������� ������
procedure TSpreadsheet.DeleteRow ( num : integer );
var i, buf : integer;
    buf2   : TRow;
begin
  if cursor.posy <> 2 then cursor.display ( -1, 0, false );
  getRow( num ).freeRowFields ( num_of_topics );
  getRow( num ).free;
  if num <> num_of_rows then
    for i := num to num_of_rows - 1 do begin
      buf2 := getRow( i + 1 );
      setRow( i, buf2 );
    end;
  dec ( num_of_rows );
  buf := ( num_of_rows div 8 );
  if ( num_of_rows mod 8 <> 0 ) then inc ( buf );
  if buf < page_number then begin
    dec ( page_number );
    if ( num = num_of_rows + 1 ) then
      dec ( current_page );
  end;
end;

//��������, ���������� ���-�� ��������
function TSpreadsheet.getColumns():integer;
begin
  getColumns := num_of_topics;
end;

//��������, ���������� �������� ������ �� �� �����������
function TSpreadsheet.getRowFieldValue(x, y: integer): string;
begin
  getRowFieldValue := getRow( y ).getFieldValue(x);
end;

//���������� ������
procedure TSpreadsheet.AddRow ( array_of_types : arr_types_1 );
var {$ifdef din}
    ptr    : pList;
    {$endif}

    buf    : strings;
    i,buf2 : integer;
    buf3   : TRow;
begin
  if num_of_rows div 8 >= page_number then inc(page_number);
  for i := 1 to num_of_topics do buf[i]:='';
  inc(num_of_rows);

  {$ifdef din}
  ptr := @list_of_row;
  for i := 3 to num_of_rows do
    ptr := ptr^.next;
  ptr^.next := new(pList);
  {$endif}

  buf3 := TRow.create(buf,num_of_topics,array_of_types_spr,false);
  setRow( num_of_rows, buf3 );
  buf2 := num_of_rows mod 8;
  if buf2 = 0 then inc(buf2,8);
  if page_number = current_page then getRow( num_of_rows ).display(buf2 * 3 + 1, Array_of_xCoord, num_of_topics,array_of_types);
end;

//������� �� ���� ��������
procedure TSpreadsheet.PageDown;
var i:integer;
begin
  cursor.setPosY (2);
  cursor.setPosX (1);
  inc(current_page);
  display;
end;

function TSpreadsheet.SetNumOfRows (num : integer) : integer;
begin
  num_of_rows := num;
end;

function TSpreadsheet.GetNumOfRows () : integer;
begin
  GetNumOfRows := num_of_rows;
end;

function Tspreadsheet.getSpr (): TStringGrid;
begin
  getSpr := Spr;
end;

procedure TSpreadsheet.SetAltColor ( Col : longint );
begin
  Alter_color := Col;
end;

procedure TSpreadsheet.SetColor ( Col : longint );
begin
  color := Col;
end;

procedure TSpreadsheet.SetFixColor ( Col : longint );
begin
  fix_color := Col;
end;

procedure TSpreadsheet.SetFontColor ( Col : longint );
begin
  font_color := Col;
end;

function TMaster_str.compare():boolean;
  begin

  end;


end.

