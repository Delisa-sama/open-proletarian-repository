#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <list>

char* fun() {

	DIR * dir = 0;
	
	dir = opendir("./");
	
	struct dirent * file = 0;
	
	int m_pipe[2] = { 0, 0};	
	int d_pipe[2] = { 0, 0};
	
	char * min_name = new char (256);
	char full_path[256] = {"./"};
	
	//pipe(d_pipe);
	//close(d_pipe[1]);

	int p_count = 0;
	
	while ((file = readdir(dir)) != 0) {
		
		if (file -> d_type == DT_REG) {
			char * ptr = strrchr(file -> d_name,'.');
			char * ptr2 = 0;
			
			if (ptr != 0)
				if ((ptr2 = strrchr(min_name,'.')) == 0)
					strcpy(min_name, file -> d_name);
				else if (strlen(ptr) < strlen(ptr2))
					strcpy(min_name, file -> d_name);
		}
		
		if (file -> d_type == DT_DIR && strcmp(file -> d_name, ".") != 0 && strcmp(file -> d_name, "..") != 0) {
			int pid;
			
			if (!d_pipe[0]) {
				pipe(d_pipe);
			}
			
			pid = fork();
			
			if (!pid) {
				close(d_pipe[0]);

				m_pipe[0] = d_pipe[0];
				m_pipe[1] = d_pipe[1];
				
				d_pipe[0] = 0;
				d_pipe[1] = 0;
				
				char dir_path[256];
				
				strcpy(dir_path, file -> d_name);
				closedir(dir);
				
				strcat(full_path, dir_path);
				strcat(full_path, "/");
				
				dir = opendir(full_path);
				
				p_count = 0;
			}//derived
			else { 
				if (d_pipe[1]) {
				
					close(d_pipe[1]);
					d_pipe[1];
				}
				p_count++;
			}//parent
		}
	}
		
	closedir(dir);
	
	int stat;
    pid_t id;
	
    while((p_count != 0) && ((id = waitpid(0, &stat, 0)) > 0)) {
		
		p_count--;
		
        if(!WIFEXITED(stat)) {
			perror("Proccess error");
            return 0;
        }
    }
		
	char buf_str[256] = {"\0"};
	char * buf_ptr = &(buf_str[0]);
	
	
	if (d_pipe[0])
		while (read(d_pipe[0], buf_ptr, 1)) {
			if (*buf_ptr == '\0') {
				buf_ptr = &(buf_str[0]);
				char * ptr = strrchr(buf_str,'.');
				char * ptr2 = 0;
				
				if (ptr != 0)
					if ((ptr2 = strrchr(min_name,'.')) == 0)
						strcpy(min_name, buf_str);
					else if (strlen(ptr) < strlen(ptr2))
						strcpy(min_name, buf_str);
			}
			else 
				buf_ptr++;
				printf("*");//, *buf_ptr);
			
		}
	
	if (m_pipe[1]) {
 		printf("%d\n", write(m_pipe[1], min_name, strlen(min_name) + 1));
		
		close(m_pipe[1]);
		close(d_pipe[0]);
	
		return 0;
	}
	
	close(m_pipe[1]);
	close(d_pipe[0]);
	
	return min_name;
}


int main () {

	char * str = fun();
	
	if (str != 0) { 
		printf("%s\n",str);	
		delete [] str;
	}
	
	return 0;
}