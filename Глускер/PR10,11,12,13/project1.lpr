program project1;
//PR 10 1 5
// ���� ������ �ਫ������. ����: ��������, ࠧ��� (� ��), �⮨�����, �ந�����⥫�, ३⨭�.
//����� �ந�����⥫� ��।������ ���������, ���ᮬ �����஭��� �����.
uses
  crt;

const
  max = 10;//���ᨬ��쭮� ���-�� �ਫ������
type
  menu_item   = (add_item, File_item, listing_item, change_item, del_item,
    color_item, exit_item);
  //�㭪�� ����:
  //add_item [COMPLETED]
  //File_item [COMPLETED]
  //listing_item [COMPLETED]
  //change_item [COMPLETED]
  //del_item [COMPLETED]
  //color_item[COMPLETED]
  //exit_item [COMPLETED]
  application = record  //������ �����       [152]
    Name: string[32];   //��������            [32]
    Size: real;         //������, Mb          [50]
    Cost: integer;      //�⮨�����, Gold     [5]
    Producer: record
      Name: string[32]; //��� ࠧࠡ��稪�    [32]
      EMail: string[32];//EMail ࠧࠡ��稪�  [32]
    end;
    Rating: 1..5;        //1..5, Stars        [1]
  end;                   //������ ����� 120 ����
  apps    = array [1..max] of application;//���ᨢ �ਫ������
  tTextColor = 0..15;//����
  tFile   = file of application;
  ntFile  = file;
  txtFile = Text;
//��� �롮� 梥� � ����ன��� (�� 㬮�砭�� 15,10);
//call_menu ��뢠�� ����(�뢮��� �� ��࠭)
procedure call_menu(var set_select_textcolor,set_textcolor:tTextColor); forward;
//update_stars ������ �������� ������ ���-�� ����� ��५�窠��
function update_stars: byte; forward;
//app_del 㤠��� �ਫ������ �� ��
//�� �室� a - ���ᨢ ����ᥩ
//coa - ���-�� ����ᥩ � ���ᨢ�
//Number - ����� �ਫ������ ����� �㦭� 㤠����
function app_del (var a: apps; var coa: word; Number: integer): boolean;
var
  i, j: integer;
begin
  if (coa>=1) and (Number<=coa) then
  begin
    j := 1;
    for i := 1 to coa do
    begin
      a[j] := a[i];
      if i<>Number then
        Inc(j);
    end;
    Dec(coa);
  end
  else
    app_del := False;
end;

function update_stars: byte;
var
  i:  byte;
  c2: char;
begin
  writeln;
  Write('*');
  update_stars := 1;
  repeat
    c2 := readkey;
    if c2 = #0 then
    begin
      c2 := readkey;
      if c2 = #75 then
      begin
        if update_stars = 1 then
          update_stars := 5
        else
          Dec(update_stars);
        delline;
        insline;
        gotoxy(1, wherey);
        for i := 1 to update_stars do
          Write('*');
      end;
      if c2 = #77 then
      begin
        if update_stars = 5 then
          update_stars := 1
        else
          Inc(update_stars);
        delline;
        insline;
        gotoxy(1, wherey);
        for i := 1 to update_stars do
          Write('*');
      end;
    end;
  until c2 = #13;
end;
//app_del_menu �㦭�, ��� ⮣� �⮡� ���짮��⥫� ��ࠫ �ਫ������ ��� 㤠�����, �᫨ ��� ����
//�� �室� a - ���ᨢ ����ᥩ
//coa - ���-�� ����ᥩ � ���ᨢ�
//Number - ����� �ਫ������ ����� �㦭� 㤠����
procedure app_del_menu (var a: apps; var coa: word; var Number: integer);
var
  i: integer;
begin
  clrscr;
  if coa>=1 then
  begin
    writeln('ID     Name');
    for i := 1 to coa do
    begin
      writeln(i: 2, '     ', a[i].Name);
      if i mod 22 = 0 then
      begin
        writeln;
        Write('Press any key...');
        readln;
      end;
    end;
    writeln;
    Write('Enter ID of App: ');
    readln(Number);
    if (Number<1)or(Number>coa) then
    begin
      writeln('Error, ID not found');
      writeln('Press any key...');
      readln;
      clrscr;
    end
    else if app_del(a, coa, Number) then
        writeln('DELETED')
      else
        writeln('ERROR');
  end
  else
    writeln('DataBase is empty');
  delay(1000);
end;
//app_change � 楫�� �������� �������� �� ����, �� �������饩 �����
//�� �室� a - ���ᨢ ����ᥩ
//coa - ���-�� ����ᥩ � ���ᨢ�
//����७���(�������) ����ணࠬ��: app_list_menu � app_list_field_menu ���ᠭ� ��� �� ��������
procedure app_change (var a: apps; coa: word;set_select_textcolor,set_textcolor:tTextColor);
var
  NumApp, i: integer;
  error:     word;
  //app_list_menu �������� ����� ������ ��� ���������
  //�� �室� a - ���ᨢ ����ᥩ
  //coa - ���-�� ����ᥩ � ���ᨢ�
  //��室 NumApp - ����� �㦭�� �����
  procedure app_list_menu (var a: apps; coa: word; var NumApp: integer);
  var
    i: integer;
  begin
    if coa>=1 then
    begin
      writeln('ID     Name');
      for i := 1 to coa do
      begin
        writeln(i: 2, '     ', a[i].Name);
        if i mod 22 = 0 then
        begin
          writeln;
          Write('Press any key...');
          readln;
        end;
      end;
      writeln;
      Write('Enter ID of App: ');
      readln(NumApp);
      if (NumApp<1)or(NumApp>coa) then
      begin
        writeln('Error, ID not found');
        writeln('Press any key...');
        readln;
      end;
    end
    else
      writeln('DataBase is empty');
    delay(1000);
    clrscr;
  end;
  //app_list_field_menu �������� ����� ���� ��� ���������
  //�� �室� a - ���ᨢ ����ᥩ
  //NumApp - ����� �㦭�� �����
  //����७��� (�������) ����ணࠬ��: blink_to_field � resetstr_f ���ᠭ� ��� �� ��������
  function app_list_field_menu (var a: apps; NumApp: integer;set_select_textcolor,set_textcolor:tTextColor): integer;
  var
    i:integer;
    c: char;
    procedure case_list_menu(pos:byte; var a:apps;NumApp:integer);
    var i: integer;
    begin
      case pos of
        0: Write('Name            : ', a[NumApp].Name);
        1: Write('Size            : ', a[NumApp].Size: 10: 2);
        2: Write('Cost            : ', a[NumApp].Cost);
        3: Write('Producer Name   : ', a[NumApp].Producer.Name);
        4: Write('Producer EMail  : ', a[NumApp].Producer.EMail);
        5:
        begin
          Write('Raiting         : ');
          for i := 1 to a[NumApp].Rating do
            Write('*');
        end;
      end;
    end;

    //blink_to_field �㦭� ��� ��㠫쭮�� �⮡ࠦ���� ⥪�饣� �롮�
    //�� �室� pos - ⥪��� ������ ����� �� Y
    //�� �室� a - ���ᨢ ����ᥩ
    //NumApp - ����� �㦭�� �����
    procedure blink_to_field (pos: byte; var a: apps; NumApp: integer;set_select_textcolor,set_textcolor:tTextColor);
    begin
      gotoxy(1, pos+1);
      delline;
      insline;
      textcolor(set_select_textcolor);
      case_list_menu(pos,a,NumApp);
      textcolor(set_textcolor);
    end;
    //resetstr_f �㦭� ��� ���㠫쭮�� ������ ��諮�� �롮� � ��室��� ���������
    //�� �室� pos - ⥪��� ������ ����� �� Y
    //�� �室� a - ���ᨢ ����ᥩ
    //NumApp - ����� �㦭�� �����
    procedure resetstr_f (pos: byte; var a: apps; NumApp: integer;set_select_textcolor,set_textcolor:tTextColor);
    begin
      gotoxy(1, pos+1);
      delline;
      insline;
      textcolor(set_textcolor);
      case_list_menu(pos,a,NumApp);
      textcolor(set_select_textcolor);
    end;

  begin
    clrscr;
    gotoxy(1, 1);
    textcolor(set_select_textcolor);
    Writeln('Name            : ', a[NumApp].Name);
    textcolor(set_textcolor);
    writeln('Size            : ', a[NumApp].Size: 10: 2);
    Writeln('Cost            : ', a[NumApp].Cost);
    Writeln('Producer Name   : ', a[NumApp].Producer.Name);
    Writeln('Producer EMail  : ', a[NumApp].Producer.EMail);
    Write('Raiting         : ');
    for i := 1 to a[NumApp].Rating do
      Write('*');
    app_list_field_menu := 0;
    repeat
      c := readkey;
      if c = #0 then
      begin
        c := readkey;
        if c = #72 then
        begin
          resetstr_f(app_list_field_menu, a, NumApp,set_select_textcolor,set_textcolor);
          if app_list_field_menu = 0 then
            app_list_field_menu := 5
          else
            Dec(app_list_field_menu);
          blink_to_field(app_list_field_menu, a, NumApp,set_select_textcolor,set_textcolor);
        end;
        if c = #80 then
        begin
          resetstr_f(app_list_field_menu, a, NumApp,set_select_textcolor,set_textcolor);
          if app_list_field_menu = 5 then
            app_list_field_menu := 0
          else
            Inc(app_list_field_menu);
          blink_to_field(app_list_field_menu, a, NumApp,set_select_textcolor,set_textcolor);
        end;
      end;
    until (c = #13)or(c = #27);
    if c = #27 then app_list_field_menu:=-1;
    gotoxy(1, 10);
  end;

begin
  clrscr;
  gotoxy(1, 1);
  if coa>0 then
  begin
    app_list_menu(a, coa, NumApp);
    case app_list_field_menu(a, NumApp,set_select_textcolor,set_textcolor) of
      0:
      begin
        gotoxy(1, 8);
        {$I-}
        repeat
          Write('  Enter the name of the appication: ');
          readln(a[NumApp].Name);
          error := ioresult;
          if (NumApp>1)and(error = 0) then
            for i := 1 to NumApp-1 do
              if a[i].Name = a[NumApp].Name then
                error := 1;
        until (a[NumApp].Name<>'')and(error = 0);
      end;
      1:
      begin
        gotoxy(1, 8);
        repeat
          Write('  Enter the application size(Mb): ');
          readln(a[NumApp].Size);
          error := ioresult;
        until (a[NumApp].Size>0)and(a[NumApp].Size<1024000)and(error = 0);
      end;
      2:
      begin
        gotoxy(1, 8);
        repeat
          Write('  Enter the cost of the application: ');
          readln(a[NumApp].Cost);
          error := ioresult;
        until (a[NumApp].Cost>=0)and(a[NumApp].Cost<32767)and(error = 0);
      end;
      3:
      begin
        gotoxy(1, 8);
        repeat
          Write('  Enter the name of the application vendor: ');
          readln(a[NumApp].Producer.Name);
          error := ioresult;
        until (a[NumApp].Producer.Name<>'')and(error = 0);
      end;
      4:
      begin
        gotoxy(1, 8);
        repeat
          Write('  Enter the EMail of the application vendor: ');
          readln(a[NumApp].Producer.EMail);
          error := ioresult;
        until (a[NumApp].Producer.EMail<>'')and(error = 0);
        {$I+}
      end;
      5:
      begin
        gotoxy(1, 8);
        Write('  Use arrows to select the application rating(1..5 stars): ');
        a[coa].Rating := update_stars;
      end;
    end;
    writeln('OK');
  end
  else
    writeln('Apps not found');
  delay(1000);
end;
//app_add �������� ��������� ���� �ਫ������ � ��
//�� �室� a - ���ᨢ ����ᥩ
//coa - ���-�� ����ᥩ � ���ᨢ�
//�����頥� ���祭�� ⨯� boolean (T - ����� 㤠���� ��������, F - �᫨ �������� �஡����)
function app_add (var a: apps; var coa: word): boolean;
var
  i:     integer;
  FlagMore: boolean;
  more_please: char;
  error: word;
begin
  repeat
    clrscr;
    gotoxy(1, 1);
    FlagMore := False;
    Inc(coa);//���-�� �ਫ������ � ���� +1
    if coa<=max then
    begin
      {$I-}
      repeat
        Write('Enter the name of the appication: ');
        readln(a[coa].Name);
        error := ioresult;
        if (coa>1)and(error = 0)and(length(a[coa].Name)<=32) then
          for i := 1 to coa-1 do
            if a[i].Name = a[coa].Name then
              error := 1;
      until (a[coa].Name<>'')and(error = 0)and(length(a[coa].Name)<=32);
      repeat
        Write('Enter the application size(Mb): ');
        readln(a[coa].Size);
        error := ioresult;
      until (a[coa].Size>0)and(a[coa].Size<1024000)and(error = 0);
      repeat
        Write('Enter the cost of the application: ');
        readln(a[coa].Cost);
        error := ioresult;
      until (a[coa].Cost>=0)and(a[coa].Cost<32767)and(error = 0);
      repeat
        Write('Enter the name of the application vendor: ');
        readln(a[coa].Producer.Name);
        error := ioresult;
      until (a[coa].Producer.Name<>'')and(error = 0)and
        (length(a[coa].Producer.Name)<=32);
      ;
      repeat
        Write('Enter the EMail of the application vendor: ');
        readln(a[coa].Producer.EMail);
        error := ioresult;
      until (a[coa].Producer.EMail<>'')and(error = 0)and
        (length(a[coa].Producer.EMail)<=32);
      {$I+}
      Write('Use arrows to select the application rating(1..5 stars): ');
      a[coa].Rating := update_stars;
      writeln;
      app_add := True;
      Write('Add more? (y/n)');
      readln(more_please);
      if more_please = 'y' then
        FlagMore := True
      else
        FlagMore := False;
    end
    else
      app_add := False;
  until not(FlagMore);
  if not(app_add) then
    writeln('ERROR')
  else
  begin
    gotoxy(1, wherey+2);
    textcolor(10);
    Write('OK');
  end;
  delay(2000);
end;
//app_list �������� ��ᬮ���� ᯨ᮪ �ਫ������ �᫨ ��� ����
//�� �⠭����� ��࠭ crt ����頥��� �� 3 �����, ���⮬� ࠧࠡ��稪 ������� ���:
//�᫨ ����ᥩ > 3, � ��᫥ �뢮�� ����� 3-�, �� ����⨨ �� ���� ������� ���� �뢮������ ᫥���騥,
//�� �� ���, ���� ���� �����
//�� �室� a - ���ᨢ ����ᥩ
//coa - ���-�� ����ᥩ � ���ᨢ�
//����७���(�����쭠�) ����ணࠬ��: line ���ᠭ� ��� �� ��������
procedure app_list (var a: apps; var coa: word);
var
  i, j, k:  integer;
  FlagPage: boolean;
  //line �뢮��� �� ��࠭ ��ப� �� 75 ����ᮢ � ��⪠�� �� �����:
  //#---------------------------------------------------------------------------#
  procedure line;
  var
    i: integer;
  begin
    gotoxy(1, wherey+1);
    Write('#');
    for i := 1 to 75 do
      Write('-');
    Writeln('#');
  end;

begin
  clrscr;
  k := 1;
  FlagPage := False;
  if coa>=1 then
  begin
    for i := 1 to coa do
    begin
      line;
      Write('| ');
      Write('Name of app: ', a[i].Name);
      gotoxy(76, wherey);
      Writeln(' |');
      Write('| ');
      Write('Size of app: ', a[i].Size: 2: 2, ' Mb');
      gotoxy(76, wherey);
      Writeln(' |');
      Write('| ');
      Write('Cost of app: ', a[i].Cost, ' Gold');
      gotoxy(76, wherey);
      Writeln(' |');
      Write('| ');
      Write('Name of producer of the app: ', a[i].Producer.Name);
      gotoxy(76, wherey);
      Writeln(' |');
      Write('| ');
      Write('Email of producer of app: ', a[i].Producer.EMail);
      gotoxy(76, wherey);
      Writeln(' |');
      Write('| ');
      Write('Name of app: ');
      for j := 1 to a[i].Rating do
        Write('*');
      gotoxy(76, wherey);
      Write(' |');
      line;
      if (i mod 3 = 0) and (i<coa) or (FlagPage) then
      begin
        FlagPage := True;
        writeln;
        gotoxy(66, wherey);
        if FlagPage then
          writeln('Page ', k, '...', coa div 3+1);
        Inc(k);
        Write('Press any key...');
        readln;
        clrscr;
      end;
    end;
    if coa<=3 then
    begin
      Write('Press any key...');
      readln;
      clrscr;
    end;
  end
  else
  begin
    writeln('DataBase is Empty');
    delay(1000);
  end;
end;
//menu �������� �����⢮���� �� �������� ���� �ணࠬ��
//�� �室� a - ���ᨢ ����ᥩ
//coa - ���-�� ����ᥩ � ���ᨢ�
//����७���(�������) ����ணࠬ��: blink_to_item � resetstr ���ᠭ� ��� �� ��������
function menu(var set_select_textcolor,set_textcolor:tTextColor): menu_item;
var
  c: char;

  function strmenu (pos: byte): string;
  begin
    case pos of
      0: strmenu := 'ADD';
      1: strmenu := 'File Menu';
      2: strmenu := 'Listing';
      3: strmenu := 'Change';
      4: strmenu := 'Delete';
      5: strmenu := 'Colors';
      6: strmenu := 'Exit';
    end;
  end;

  //blink_to_item �������� ���㠫쭮 ��४������� �� ���� �㭪� ����
  //�� �室� pos - ⥪��� ������ ����� �� Y
  procedure blink_to_item (pos: byte;set_select_textcolor,set_textcolor:tTextColor);
  begin
    gotoxy(1, pos+1);
    delline;
    insline;
    textcolor(set_select_textcolor);
    Write(strmenu(pos));
    textcolor(set_textcolor);
  end;
  //resetstr �������� ������ � ��室��� ��������� ࠭�� ��࠭�� �㭪� ����
  //�� �室� pos - ⥪��� ������ ����� �� Y
  procedure resetstr (pos: byte;set_select_textcolor,set_textcolor:tTextColor);
  begin
    gotoxy(1, pos+1);
    delline;
    insline;
    textcolor(set_textcolor);
    Write(strmenu(pos));
    textcolor(set_select_textcolor);
  end;

begin
  menu := add_item;
  repeat
    c := readkey;
    if c = #0 then
    begin
      c := readkey;
      if c = #72 then
      begin
        resetstr(Ord(menu),set_select_textcolor,set_textcolor);
        if menu = add_item then
          menu := exit_item
        else
          menu := pred(menu);
        blink_to_item(Ord(menu),set_select_textcolor,set_textcolor);
      end;
      if c = #80 then
      begin
        resetstr(Ord(menu),set_select_textcolor,set_textcolor);
        if menu = exit_item then
          menu := add_item
        else
          menu := succ(menu);
        blink_to_item(Ord(menu),set_select_textcolor,set_textcolor);
      end;
    end;
  until c = #13;
end;
//call_menu ��뢥� ������� ����
procedure change_textcolor (var set_textcolor, set_select_textcolor: tTextColor);
type
  change1 = 1..2;
var
  c1:    change1;
  error: word;
begin
  clrscr;
  gotoxy(1, 1);
  writeln('1   Set textcolor');
  writeln('2   Set textcolor(for selector)');
  {$I-}
  repeat
    writeln('Make a choice');
    readln(c1);
    error := ioresult;
  until error = 0;
  clrscr;
  textcolor(0);
  textbackground(LightGray);
  writeln('Black           0');
  textbackground(0);
  textcolor(1);
  writeln('Blue            1');
  textcolor(2);
  writeln('Green           2');
  textcolor(3);
  writeln('Cyan            3');
  textcolor(4);
  writeln('Red             4');
  textcolor(5);
  writeln('Magenta         5');
  textcolor(6);
  writeln('Brown           6');
  textcolor(7);
  writeln('LightGray       7');
  textcolor(8);
  writeln('DarkGray        8');
  textcolor(9);
  writeln('LightBlue       9');
  textcolor(10);
  writeln('LightGreen     10');
  textcolor(11);
  writeln('LightCyan      11');
  textcolor(12);
  writeln('Lightred       12');
  textcolor(13);
  writeln('LightMagenta   13');
  textcolor(14);
  writeln('Yellow         14');
  textcolor(15);
  writeln('White          15');
  textcolor(set_textcolor);
  textbackground(0);
  case c1 of
    1: repeat
        writeln('Select textcolor');
        readln(set_textcolor);
        error := ioresult;
      until (error = 0) and (set_textcolor<>set_select_textcolor);
    2: repeat
        writeln('Select textcolor(for select)');
        readln(set_select_textcolor);
        error := ioresult;
      until (error = 0) and (set_textcolor<>set_select_textcolor);
  end;
  {$I+}
end;
//tf_save ��࠭�� �� �����, �᫨ ��� ����, � ⨯���஢���� 䠩�
//�� �室� f(tFile) - ⨯���஢���� 䠩�(application)
//         a(apps) - ���ᨢ ����ᥩ(application)
//         coa(word) - ���-�� ����ᥩ
//������� True, �᫨ ��࠭��� �ᯥ譮
function tf_save (var f: tFile; var a: apps; coa: word): boolean;
var
  FileName: string;
  i: integer;
begin
  clrscr;
  gotoxy(1, 1);
  if coa>=1 then
  begin
    i := 1;
    repeat
      writeln('Enter filename');
      {$I-}
      readln(FileName);
      FileName := FileName+'.dat';
      Assign(f, FileName);
      Rewrite(f);
    until FileName<>'';
    {$I+}
    for i := 1 to coa do
      Write(f, a[i]);
    Close(f);
    tf_save := True;
    writeln('Save complete');
  end
  else
  begin
    writeln('DataBase is empty');
    tf_save := False;
  end;
  delay(1000);
end;
//tf_load ����㦠�� �� ����� �� ⨯���஢������ 䠩�� � ���ᨢ ����ᥩ
//�� �室�  f(tFile) - ⨯���஢���� 䠩�(application)
//�� ��室� a(apps) - ���ᨢ ����ᥩ(application)
//          coa(word) - ���-�� ����ᥩ
//������� True, �᫨ ��࠭��� �ᯥ譮
function tf_load (var f: tFile; var a: apps; var coa: word): boolean;
var
  FileName: string;
  error: word;
  i: integer;

begin
  clrscr;
  gotoxy(1, 1);
  i := 1;
  repeat
    writeln('Enter filename');
    readln(FileName);
  until FileName<>'';
  FileName := FileName+'.dat';
  {$I-}
  Assign(f, FileName);
  reset(f);
  error := ioresult;
  {$I+}
  if error = 0 then
  begin
    i := 1;
    while not(EOF(f)) do
    begin
      Read(f, a[i]);
      Inc(i);
    end;
    coa := i-1;
    seek(f, filepos(f)-1);
    Close(f);
    tf_load := True;
  end
  else
  begin
    writeln('File not found');
    tf_load := False;
  end;
  delay(1000);
end;
//ntf_save ��࠭�� �� �����, �᫨ ��� ����, � ⨯���஢���� 䠩�
//�� �室� f(tFile) - ��⨯���஢���� 䠩�(application)
//         a(apps) - ���ᨢ ����ᥩ(application)
//������� True, �᫨ ��࠭��� �ᯥ譮
//coa(word) - ���-�� ����ᥩ
function ntf_save (var f: ntFile; var a: apps; coa: word): boolean;
var
  FileName: string;
  i: integer;
  result_f, error: word;
begin
  clrscr;
  gotoxy(1, 1);
  if coa>=1 then
  begin
    i := 1;
    repeat
      writeln('Enter filename');
      {$I-}
      readln(FileName);
      if not(i+3<length(FileName)) then
        FileName := FileName+'.nt';
      Assign(f, FileName);
      Rewrite(f, 2);
    until FileName<>'';
    {$I+}
    BlockWrite(f, coa, 1, Result_f);
    for i := 1 to coa do
      BlockWrite(f, a[i], 60, result_F);
    Close(f);
    ntf_save := True;
  end
  else
  begin
    writeln('DataBase is empty');
    ntf_save := False;
  end;
  delay(700);
end;
//ntf_load ����㦠�� �� ����� �� ��⨯���஢������ 䠩�� � ���ᨢ ����ᥩ
//� ����� 2-� ����� ᮤ�ন��� �᫮ ����ᥩ, ����ᠭ��� � 䠩�(integer)
//����� ���� ����� �� 120 ���� ������(60 ������ �� 2)
//�� �室�  f(tFile) - ��⨯���஢���� 䠩�(application)
//�� ��室� a(apps) - ���ᨢ ����ᥩ(application)
//          coa(word) - ���-�� ����ᥩ
//������� True, �᫨ ��࠭��� �ᯥ譮
function ntf_load (var f: ntFile; var a: apps; var coa: word): boolean;
var
  FileName: string;
  error, result_f: word;
  i, j:     integer;

begin
  clrscr;
  gotoxy(1, 1);
  i := 1;
  repeat
    writeln('Enter filename');
    readln(FileName);
  until FileName<>'';
  FileName := FileName+'.nt';
  {$I-}
  Assign(f, FileName);
  reset(f, 2);
  error := ioresult;
  if error = 0 then
  begin
    BlockRead(f, coa, 1, Result_f);
    if Result_f<>0 then
    begin
      i := 2;
      j := 1;
      while j<=coa do
      begin
        BlockRead(f, a[j], 60, Result_f);
        Inc(j);
      end;
      error := ioresult;
    {$I+}
      Close(f);
      ntf_load := True;
    end
    else
    begin
      writeln('ERROR');
      ntf_load := False;
    end;
  end
  else
  begin
    writeln('File not found');
    ntf_load := False;
  end;
  delay(3000);
end;
//txtf_save ��࠭�� �� �����, �᫨ ��� ����, � ��⨯���஢���� 䠩�
//�� �室� f(tFile) - ��⨯���஢���� 䠩�(application)
//         a(apps) - ���ᨢ ����ᥩ(application)
//������� True, �᫨ ��࠭��� �ᯥ譮
//coa(word) - ���-�� ����ᥩ
function txtf_save (var f: txtFile; var a: apps; coa: word): boolean;
var
  filename: string;
  i: integer;
begin
  clrscr;
  gotoxy(1, 1);
  if coa>=1 then
  begin
    i := 1;
    repeat
      writeln('Enter filename');
      {$I-}
      readln(FileName);
      FileName := FileName+'.csv';
      Assign(f, FileName);
      Rewrite(f);
    until FileName<>'';
    {$I+}
    for i := 1 to coa do
    begin
      writeln(f, a[i].Name);
      writeln(f, a[i].size);
      writeln(f, a[i].Cost);
      writeln(f, a[i].Producer.Name);
      writeln(f, a[i].Producer.EMail);
      if i = coa then
        Write(f, a[i].Rating)
      else
        writeln(f, a[i].Rating);
    end;
    Close(f);
    txtf_save := True;
  end
  else
    txtf_save := False;
end;
//txtf_load ��࠭�� �� �����, �᫨ ��� ����, � ⨯���஢���� 䠩�
//�� �室� f(tFile) - ��⨯���஢���� 䠩�(application)
//         a(apps) - ���ᨢ ����ᥩ(application)
//������� True, �᫨ ��࠭��� �ᯥ譮
//coa(word) - ���-�� ����ᥩ
function txtf_load (var f: txtFile; var a: apps; var coa: word): boolean;
var
  filename: string;
  error:    word;
begin
  clrscr;
  gotoxy(1, 1);
  repeat
    writeln('Enter filename');
    readln(FileName);
  until FileName<>'';
  FileName := FileName+'.csv';
  {$I-}
  Assign(f, FileName);
  reset(f);
  error := ioresult;
  if error = 0 then
  begin
    if seekeof(f) then
      coa := 0
    else
      coa := 1;
    while not(seekeof(f)) do
    begin
      while not(seekeoln(f)) do
        Read(f, a[coa].Name);
      readln(f);
      while not(seekeoln(f)) do
        Read(f, a[coa].Size);
      readln(f);
      while not(seekeoln(f)) do
        Read(f, a[coa].Cost);
      readln(f);
      while not(seekeoln(f)) do
        Read(f, a[coa].Producer.Name);
      readln(f);
      while not(seekeoln(f)) do
        Read(f, a[coa].Producer.EMail);
      readln(f);
      while not(seekeoln(f)) do
        Read(f, a[coa].Rating);
      readln(f);
      error := ioresult;
  {$I+}
      if error = 0 then
        Inc(coa)
      else
        writeln('ERROR');
    end;
    Dec(coa);
    txtf_load := True;
  end
  else
    txtf_load := False;
end;
//file_menu_items � ��� ���ᠭ� �� �㭪�� ����, � ⠪ �� ��楤��� ����᪮�� ��४��祭�� ����� ����
procedure file_menu_items(set_select_textcolor,set_textcolor:tTextColor;var a:apps; var coa:word);
type
  file_menu_items = (typed_item, text_item, untyped_item, exit_item);
var
  c2:    1..2;
  error: word;
  FlagTyped, FlagUntyped, FlagTXT: boolean;
  //file_menu �������� ����� �㭪� 䠩������ ����
  //�����頥� ��࠭�� �㭪� ����
  function file_menu(var set_select_textcolor,set_textcolor:tTextColor): file_menu_items;
  var
    c: char;
    //strmenu_f ���⠥� ⥪�騩 �㭪� ����
    //�� �室� pos - ⥪��� ������ ����� �� Y
    function strmenu_f (pos: byte): string;
    begin
      case pos of
        0: strmenu_f := 'Typed file';
        1: strmenu_f := 'Text file';
        2: strmenu_f := 'Untyped file';
        3: strmenu_f := 'Exit';
      end;
    end;

    //blink_to_item_fm �������� ���㠫쭮 ��४������� �� ���� �㭪� ����
    //�� �室� pos - ⥪��� ������ ����� �� Y
    procedure blink_to_item_fm (pos: byte;set_select_textcolor,set_textcolor:tTextColor);
    begin
      gotoxy(1, pos+1);
      delline;
      insline;
      textcolor(set_select_textcolor);
      Write(strmenu_f(pos));
      textcolor(set_textcolor);
    end;
    //resetstr_fm �������� ������ � ��室��� ��������� ࠭�� ��࠭�� �㭪� ����
    //�� �室� pos - ⥪��� ������ ����� �� Y
    procedure resetstr_fm (pos: byte;set_select_textcolor,set_textcolor:tTextColor);
    begin
      gotoxy(1, pos+1);
      delline;
      insline;
      textcolor(set_textcolor);
      Write(strmenu_f(pos));
      textcolor(set_select_textcolor);
    end;

  begin
    file_menu := typed_item;
    repeat
      c := readkey;
      if c = #0 then
      begin
        c := readkey;
        if c = #72 then
        begin
          resetstr_fm(Ord(file_menu),set_select_textcolor,set_textcolor);
          if file_menu = typed_item then
            file_menu := exit_item
          else
            file_menu := pred(file_menu);
          blink_to_item_fm(Ord(file_menu),set_select_textcolor,set_textcolor);
        end;
        if c = #80 then
        begin
          resetstr_fm(Ord(file_menu),set_select_textcolor,set_textcolor);
          if file_menu = exit_item then
            file_menu := typed_item
          else
            file_menu := succ(file_menu);
          blink_to_item_fm(Ord(file_menu),set_select_textcolor,set_textcolor);
        end;
      end;
    until c = #13;
  end;

var
  FlagExit: boolean;
  //call_file_menu ��뢠�� 䠩����� ����
  procedure call_file_menu(set_select_textcolor,set_textcolor:tTextColor);
  begin
    clrscr;
    gotoxy(1, 1);
    textcolor(set_select_textcolor);
    writeln('Typed file');
    textcolor(set_textcolor);
    writeln('Text file');
    writeln('Untyped file');
    writeln('Exit');
  end;

  procedure save_load_file_menu(var a:apps;var coa:word);
  var tf:     tFile;
  ntf:    ntFile;
  txtf:   txtFile;
    procedure saved;
    begin
      gotoxy(6, 6);
      Write('Saved');
    end;

    procedure loaded;
    begin
      gotoxy(6, 6);
      Write('Loaded');
    end;

    procedure error_f;
    begin
      gotoxy(6, 6);
      Write('ERROR');
    end;

  begin
    writeln('File menu');
    writeln('1  Save');
    writeln('2  Load');
          {$I-}
    readln(c2);
    error := ioresult;
          {$I+}
    if error = 0 then
      if c2 = 1 then
      begin
        if FlagTyped then
          if tf_save(tf, a, coa) then
            saved
          else
            error_f;
        if FlagUntyped then
          if ntf_save(ntf, a, coa) then
            saved
          else
            error_f;
        if FlagTXT then
          if txtf_save(txtf, a, coa) then
            saved
          else
            error_f;
      end
      else
      begin
        if FlagTyped then
          if tf_load(tf, a, coa) then
            loaded
          else
            error_f;
        if FlagUntyped then
          if ntf_load(ntf, a, coa) then
            loaded
          else
            error_f;
        if FlagTXT then
          if txtf_load(txtf, a, coa) then
            loaded
          else
            error_f;
      end;
    delay(700);
  end;

begin
  FlagExit := False;
  repeat
    call_file_menu(set_select_textcolor,set_textcolor);
    FlagTyped := False;
    FlagUntyped := False;
    FlagTXT := False;
    case file_menu(set_select_textcolor,set_textcolor) of
      typed_item:
      begin
        clrscr;
        gotoxy(1, 1);
        FlagTyped := True;
        save_load_file_menu(a,coa);
      end;
      text_item:
      begin
        clrscr;
        gotoxy(1, 1);
        FlagTXT := True;
        save_load_file_menu(a,coa);
      end;
      untyped_item:
      begin
        clrscr;
        gotoxy(1, 1);
        FlagUntyped := True;
        save_load_file_menu(a,coa);
      end;
      exit_item:
        FlagExit := True;
    end;
  until FlagExit;
end;
//call_menu �㦭� ��� �맮�� main menu
procedure call_menu(var set_select_textcolor,set_textcolor:tTextColor);
begin
  clrscr;
  textcolor(set_select_textcolor);
  gotoxy(13, 7);
  writeln('::::    ::::      :::     ::::::::::: ::::    :::      ::::    ::::  :::::::::: ::::    ::: :::    ::: ');
  gotoxy(12, 8);
  writeln('+:+:+: :+:+:+   :+: :+:       :+:     :+:+:   :+:      +:+:+: :+:+:+ :+:        :+:+:   :+: :+:    :+:');
  gotoxy(11, 9);
  writeln('+:+ +:+:+ +:+  +:+   +:+      +:+     :+:+:+  +:+      +:+ +:+:+ +:+ +:+        :+:+:+  +:+ +:+    +:+');
  gotoxy(10, 10);
  writeln('+#+  +:+  +#+ +#++:++#++:     +#+     +#+ +:+ +#+      +#+  +:+  +#+ +#++:++#   +#+ +:+ +#+ +#+    +:+');
  gotoxy(9, 11);
  writeln('+#+       +#+ +#+     +#+     +#+     +#+  +#+#+#      +#+       +#+ +#+        +#+  +#+#+# +#+    +#+ ');
  gotoxy(8, 12);
  writeln('#+#       #+# #+#     #+#     #+#     #+#   #+#+#      #+#       #+# #+#        #+#   #+#+# #+#    #+#');
  gotoxy(7, 13);
  writeln('###       ### ###     ### ########### ###    ####      ###       ### ########## ###    ####  ######## ');
  delay(1000);
  clrscr;
  gotoxy(1, 1);
  writeln('ADD');
  textcolor(set_textcolor);
  writeln('File Menu');
  writeln('Listing');
  writeln('Change');
  writeln('Delete');
  writeln('Colors');
  Write('Exit');
  gotoxy(1, 1);
end;
//menu_items  � ��� ���ᠭ� ����⢨� ��� ������� �㭪� �������� ����
procedure menu_items(var a:apps;var coa:word;var number:integer);
var
  FlagExit: boolean;
  set_textcolor, set_select_textcolor: tTextColor;//��� �࠭���� ⥪�饣� ���� 梥�
begin
  FlagExit := False;
  set_select_textcolor := 10;//���� ⥪�� ᥫ���� �� 㬮�砭�� 10
  set_textcolor := 15;//���� ⥪�� �� 㬮�砭�� 15
  repeat
    call_menu(set_select_textcolor,set_textcolor);
    case menu(set_select_textcolor,set_textcolor) of
      add_item:
        app_add(a, coa);
      File_item:
        file_menu_items(set_select_textcolor,set_textcolor,a,coa);
      Listing_item:
        app_list(a, coa);
      change_item:
        app_change(a, coa,set_select_textcolor,set_textcolor);
      del_item:
        app_del_menu(a, coa, Number);
      color_item:
        change_textcolor(set_textcolor, set_select_textcolor);
      exit_item:
        FlagExit := True;
    end;
  until FlagExit;
end;

var
  a:      apps;
  Number: integer;//���-�� �ਫ������
  coa:    word;


begin
  coa := 0;//��砫쭮� ���-�� ����ᥩ 0
  cursoroff;
  menu_items(a,coa,number);
end.
