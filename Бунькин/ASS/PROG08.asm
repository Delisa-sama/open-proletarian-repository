org 100h
        mov ax,3
        int 10h

        mov ax,3D00h
        mov dx, File_name
        int 21h
        jc Error_file

        mov [Handle],ax
        mov bx,ax
        mov ah,3Fh
        mov cx,0FDE8h
        mov dx,Buffer
        int 21h

        mov ah,3Eh
        mov bx,[Handle]
        int 21h

        mov dx, Mess_ok
Out_prog:
        mov ah,9
        int 21h

        int 20h

Error_file:
        mov dx, Mess_error
        jmp Out_prog

Handle dw 0
Mess_ok db 'File loaded! Start debugger!$'
Mess_error db 'Can''t open file '
File_name db 'c:\msdos.sys',0,'!$'
Buffer:
