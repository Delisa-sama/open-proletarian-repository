org 100h

Begin:

			 call Wait_key;ждем нажания  
			 cmp al,27;проверка Esc
			 je Quit_prog;если да то выход
			 cmp al,0;проверка расш.
			 je Begin;снова
			 call Out_char;вывод
			 jmp Begin;снова

Quit_prog:

			 mov al,32;пробел в al
			 call Out_char;выводим
			 int 20h;выходим

; === Подпрограммы ===
; --- Wait_key ---
Wait_key:

			 mov ah,10h;
			 int 16h
			 ret

; --- Out_char ---
Out_char:
                 
			 push cx
			 push ax
			 push es
			 push ax;сохр. регистры
			 mov ax,0B800h
			 mov es,ax;заносим сегмент видеобуффера
			 mov di,0
			 mov cx,2000
			 pop ax;восстановим код клавиши
			 mov ah,31
			 
Next_sym:
                 
			 mov [es:di],ax
			 inc di
			 inc di
			 loop Next_sym
			 pop es
			 pop ax
			 pop cx
			 ret