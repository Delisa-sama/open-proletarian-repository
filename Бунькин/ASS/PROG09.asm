org 100h

Begin:
        mov ax,3
        int 10h

        mov dx, File_name
        call Open_file
        jc Error_file

        mov bx,ax
        mov ah,3Fh   
        mov cx, Finish-100h
        mov dx, Begin     
        int 21h

        call Close_file

        mov ah,9
        mov dx, Mess_ok
        int 21h
        ret

Error_file:
        mov ah,2
        mov dl,7
        int 21h
        ret



Open_file:
        cmp [Handle],0FFFFh
        jne Quit_open
        mov ax,3D00h
        int 21h
        mov [Handle],ax
        ret
Quit_open:
        stc
        ret
Handle dw 0FFFFh

Close_file:
        mov ah,3Eh
        mov bx,[Handle]
        int 21h
        ret


File_name db 'PROG09.COM',0
Mess_ok db 'All OK!', 0Ah, 0Dh, '$'

Finish: 

