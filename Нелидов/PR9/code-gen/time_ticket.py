from ticket import Ticket


class TimeTicket(Ticket):
    """ ATTRIBUTES
    validity  (protected)
    """

    def __init__(self, validity):
        super(TimeTicket, self).__init__(validity.value * 6)
        self.validity = validity
