from ticket import Ticket


class CountTicket(Ticket):
    """ ATTRIBUTES
    count  (protected)
    """

    def __init__(self, count):
        super(CountTicket, self).__init__(count * 10)
        self.count = count
