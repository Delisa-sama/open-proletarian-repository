from enum import Enum


class Deadlines(Enum):
    ONE_DAY = 1
    FIVE_DAYS = 5
    TEN_DAYS = 10
    FIVETEEN_DAYS = 15
    ONE_MONTH = 30
    THREE_MONTH = 90
    SIX_MONTH = 180
    ONE_YEAR = 360
