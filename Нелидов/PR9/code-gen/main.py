from time_ticket import TimeTicket
from count_ticket import CountTicket
from deadlines import Deadlines

tickets = [
    TimeTicket(Deadlines.FIVE_DAYS),
    CountTicket(25),
    TimeTicket(Deadlines.ONE_MONTH),
    CountTicket(10),
    CountTicket(200),
    TimeTicket(Deadlines.ONE_YEAR)
]

for ticket in tickets:
    if ticket.cost < 300:
        print(ticket.cost)
