<?php
    require_once('db.php');
    $db = new MyDB();
    try {
        $sql = "CREATE TABLE USER (
            USER_LOGIN    TEXT PRIMARY KEY NOT NULL,
            USER_PASSWORD TEXT             NOT NULL
        );

        CREATE TABLE DONATE (
            DONATE_ID   INT PRIMARY KEY NOT NULL,
            DONATE_COST INT,
            USER_LOGIN TEXT,
            FOREIGN KEY(USER_LOGIN) REFERENCES USER(USER_LOGIN)
        );";

        $ret = $db->exec($sql);
        echo $ret;
    } finally {
        $db->close();
    }
?>
