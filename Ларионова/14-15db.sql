
CREATE TABLE ���������
( 
	position_number      integer  NOT NULL ,
	position_name        char(32)  NULL 
)
go



ALTER TABLE ���������
	ADD CONSTRAINT XPK��������� PRIMARY KEY  CLUSTERED (position_number ASC)
go



CREATE TABLE ����������
( 
	employe_number       integer  NOT NULL ,
	accept_date          datetime  NULL ,
	dismiss_date         datetime  NULL ,
	subdivision_number   integer  NOT NULL ,
	position_number      integer  NOT NULL ,
	accept_number        char(32)  NOT NULL 
)
go



ALTER TABLE ����������
	ADD CONSTRAINT XPK���������� PRIMARY KEY  CLUSTERED (accept_number ASC)
go



CREATE TABLE �������������
( 
	subdivision_number   integer  NOT NULL ,
	subdivision_name     varchar(32)  NULL 
)
go



ALTER TABLE �������������
	ADD CONSTRAINT XPK������������� PRIMARY KEY  CLUSTERED (subdivision_number ASC)
go



CREATE TABLE ����������_��������
( 
	employe_number       integer  NOT NULL ,
	sec_name             varchar(32)  NULL ,
	name                 varchar(32)  NULL ,
	mid_name             varchar(32)  NULL ,
	education            varchar(32)  NULL ,
	birth_date           datetime  NULL ,
	phone_number         char(32)  NULL ,
	accepting_date       datetime  NULL ,
	dismissal_date       datetime  NULL ,
	order_number         varchar(32)  NULL ,
	passport_number      char(32)  NULL ,
	passport_series      char(32)  NULL 
)
go



ALTER TABLE ����������_��������
	ADD CONSTRAINT XPK����������_�������� PRIMARY KEY  CLUSTERED (employe_number ASC)
go



CREATE TABLE ����
( 
	subdivision_number   integer  NOT NULL ,
	position_number      integer  NOT NULL ,
	count                integer  NULL 
)
go



ALTER TABLE ����
	ADD CONSTRAINT XPK���� PRIMARY KEY  CLUSTERED (subdivision_number ASC,position_number ASC)
go




ALTER TABLE ����������
	ADD CONSTRAINT R_7 FOREIGN KEY (employe_number) REFERENCES ����������_��������(employe_number)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ����������
	ADD CONSTRAINT R_14 FOREIGN KEY (subdivision_number,position_number) REFERENCES ����(subdivision_number,position_number)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ����
	ADD CONSTRAINT R_5 FOREIGN KEY (subdivision_number) REFERENCES �������������(subdivision_number)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ����
	ADD CONSTRAINT R_6 FOREIGN KEY (position_number) REFERENCES ���������(position_number)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




CREATE TRIGGER tD_��������� ON ��������� FOR DELETE AS
/* ERwin Builtin Trigger */
/* DELETE trigger on ��������� */
BEGIN
  DECLARE  @errno   int,
           @errmsg  varchar(255)
    /* ERwin Builtin Trigger */
    /* ���������  ���� on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="0000eb18", PARENT_OWNER="", PARENT_TABLE="���������"
    CHILD_OWNER="", CHILD_TABLE="����"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_6", FK_COLUMNS="position_number" */
    IF EXISTS (
      SELECT * FROM deleted,����
      WHERE
        /*  %JoinFKPK(����,deleted," = "," AND") */
        ����.position_number = deleted.position_number
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete ��������� because ���� exists.'
      GOTO ERROR
    END


    /* ERwin Builtin Trigger */
    RETURN
ERROR:
    raiserror @errno @errmsg
    rollback transaction
END

go


CREATE TRIGGER tU_��������� ON ��������� FOR UPDATE AS
/* ERwin Builtin Trigger */
/* UPDATE trigger on ��������� */
BEGIN
  DECLARE  @NUMROWS int,
           @nullcnt int,
           @validcnt int,
           @insposition_number integer,
           @errno   int,
           @errmsg  varchar(255)

  SELECT @NUMROWS = @@rowcount
  /* ERwin Builtin Trigger */
  /* ���������  ���� on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="00010c7f", PARENT_OWNER="", PARENT_TABLE="���������"
    CHILD_OWNER="", CHILD_TABLE="����"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_6", FK_COLUMNS="position_number" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(position_number)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,����
      WHERE
        /*  %JoinFKPK(����,deleted," = "," AND") */
        ����.position_number = deleted.position_number
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update ��������� because ���� exists.'
      GOTO ERROR
    END
  END


  /* ERwin Builtin Trigger */
  RETURN
ERROR:
    raiserror @errno @errmsg
    rollback transaction
END

go




CREATE TRIGGER tD_���������� ON ���������� FOR DELETE AS
/* ERwin Builtin Trigger */
/* DELETE trigger on ���������� */
BEGIN
  DECLARE  @errno   int,
           @errmsg  varchar(255)
    /* ERwin Builtin Trigger */
    /* ����������_��������  ���������� on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="0002c618", PARENT_OWNER="", PARENT_TABLE="����������_��������"
    CHILD_OWNER="", CHILD_TABLE="����������"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_7", FK_COLUMNS="employe_number" */
    IF EXISTS (SELECT * FROM deleted,����������_��������
      WHERE
        /* %JoinFKPK(deleted,����������_��������," = "," AND") */
        deleted.employe_number = ����������_��������.employe_number AND
        NOT EXISTS (
          SELECT * FROM ����������
          WHERE
            /* %JoinFKPK(����������,����������_��������," = "," AND") */
            ����������.employe_number = ����������_��������.employe_number
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last ���������� because ����������_�������� exists.'
      GOTO ERROR
    END

    /* ERwin Builtin Trigger */
    /* ����  ���������� on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="����"
    CHILD_OWNER="", CHILD_TABLE="����������"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_14", FK_COLUMNS="subdivision_number""position_number" */
    IF EXISTS (SELECT * FROM deleted,����
      WHERE
        /* %JoinFKPK(deleted,����," = "," AND") */
        deleted.subdivision_number = ����.subdivision_number AND
        deleted.position_number = ����.position_number AND
        NOT EXISTS (
          SELECT * FROM ����������
          WHERE
            /* %JoinFKPK(����������,����," = "," AND") */
            ����������.subdivision_number = ����.subdivision_number AND
            ����������.position_number = ����.position_number
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last ���������� because ���� exists.'
      GOTO ERROR
    END


    /* ERwin Builtin Trigger */
    RETURN
ERROR:
    raiserror @errno @errmsg
    rollback transaction
END

go


CREATE TRIGGER tU_���������� ON ���������� FOR UPDATE AS
/* ERwin Builtin Trigger */
/* UPDATE trigger on ���������� */
BEGIN
  DECLARE  @NUMROWS int,
           @nullcnt int,
           @validcnt int,
           @insaccept_number char(32),
           @errno   int,
           @errmsg  varchar(255)

  SELECT @NUMROWS = @@rowcount
  /* ERwin Builtin Trigger */
  /* ����������_��������  ���������� on child update no action */
  /* ERWIN_RELATION:CHECKSUM="0002e284", PARENT_OWNER="", PARENT_TABLE="����������_��������"
    CHILD_OWNER="", CHILD_TABLE="����������"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_7", FK_COLUMNS="employe_number" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(employe_number)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,����������_��������
        WHERE
          /* %JoinFKPK(inserted,����������_��������) */
          inserted.employe_number = ����������_��������.employe_number
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    
    IF @validcnt + @nullcnt != @NUMROWS
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update ���������� because ����������_�������� does not exist.'
      GOTO ERROR
    END
  END

  /* ERwin Builtin Trigger */
  /* ����  ���������� on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="����"
    CHILD_OWNER="", CHILD_TABLE="����������"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_14", FK_COLUMNS="subdivision_number""position_number" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(subdivision_number) OR
    UPDATE(position_number)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,����
        WHERE
          /* %JoinFKPK(inserted,����) */
          inserted.subdivision_number = ����.subdivision_number and
          inserted.position_number = ����.position_number
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    
    IF @validcnt + @nullcnt != @NUMROWS
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update ���������� because ���� does not exist.'
      GOTO ERROR
    END
  END


  /* ERwin Builtin Trigger */
  RETURN
ERROR:
    raiserror @errno @errmsg
    rollback transaction
END

go




CREATE TRIGGER tD_������������� ON ������������� FOR DELETE AS
/* ERwin Builtin Trigger */
/* DELETE trigger on ������������� */
BEGIN
  DECLARE  @errno   int,
           @errmsg  varchar(255)
    /* ERwin Builtin Trigger */
    /* �������������  ���� on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="0000f764", PARENT_OWNER="", PARENT_TABLE="�������������"
    CHILD_OWNER="", CHILD_TABLE="����"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_5", FK_COLUMNS="subdivision_number" */
    IF EXISTS (
      SELECT * FROM deleted,����
      WHERE
        /*  %JoinFKPK(����,deleted," = "," AND") */
        ����.subdivision_number = deleted.subdivision_number
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete ������������� because ���� exists.'
      GOTO ERROR
    END


    /* ERwin Builtin Trigger */
    RETURN
ERROR:
    raiserror @errno @errmsg
    rollback transaction
END

go


CREATE TRIGGER tU_������������� ON ������������� FOR UPDATE AS
/* ERwin Builtin Trigger */
/* UPDATE trigger on ������������� */
BEGIN
  DECLARE  @NUMROWS int,
           @nullcnt int,
           @validcnt int,
           @inssubdivision_number integer,
           @errno   int,
           @errmsg  varchar(255)

  SELECT @NUMROWS = @@rowcount
  /* ERwin Builtin Trigger */
  /* �������������  ���� on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="00011664", PARENT_OWNER="", PARENT_TABLE="�������������"
    CHILD_OWNER="", CHILD_TABLE="����"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_5", FK_COLUMNS="subdivision_number" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(subdivision_number)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,����
      WHERE
        /*  %JoinFKPK(����,deleted," = "," AND") */
        ����.subdivision_number = deleted.subdivision_number
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update ������������� because ���� exists.'
      GOTO ERROR
    END
  END


  /* ERwin Builtin Trigger */
  RETURN
ERROR:
    raiserror @errno @errmsg
    rollback transaction
END

go




CREATE TRIGGER tD_����������_�������� ON ����������_�������� FOR DELETE AS
/* ERwin Builtin Trigger */
/* DELETE trigger on ����������_�������� */
BEGIN
  DECLARE  @errno   int,
           @errmsg  varchar(255)
    /* ERwin Builtin Trigger */
    /* ����������_��������  ���������� on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="0000ffb1", PARENT_OWNER="", PARENT_TABLE="����������_��������"
    CHILD_OWNER="", CHILD_TABLE="����������"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_7", FK_COLUMNS="employe_number" */
    IF EXISTS (
      SELECT * FROM deleted,����������
      WHERE
        /*  %JoinFKPK(����������,deleted," = "," AND") */
        ����������.employe_number = deleted.employe_number
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete ����������_�������� because ���������� exists.'
      GOTO ERROR
    END


    /* ERwin Builtin Trigger */
    RETURN
ERROR:
    raiserror @errno @errmsg
    rollback transaction
END

go


CREATE TRIGGER tU_����������_�������� ON ����������_�������� FOR UPDATE AS
/* ERwin Builtin Trigger */
/* UPDATE trigger on ����������_�������� */
BEGIN
  DECLARE  @NUMROWS int,
           @nullcnt int,
           @validcnt int,
           @insemploye_number integer,
           @errno   int,
           @errmsg  varchar(255)

  SELECT @NUMROWS = @@rowcount
  /* ERwin Builtin Trigger */
  /* ����������_��������  ���������� on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="000122f4", PARENT_OWNER="", PARENT_TABLE="����������_��������"
    CHILD_OWNER="", CHILD_TABLE="����������"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_7", FK_COLUMNS="employe_number" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(employe_number)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,����������
      WHERE
        /*  %JoinFKPK(����������,deleted," = "," AND") */
        ����������.employe_number = deleted.employe_number
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update ����������_�������� because ���������� exists.'
      GOTO ERROR
    END
  END


  /* ERwin Builtin Trigger */
  RETURN
ERROR:
    raiserror @errno @errmsg
    rollback transaction
END

go




CREATE TRIGGER tD_���� ON ���� FOR DELETE AS
/* ERwin Builtin Trigger */
/* DELETE trigger on ���� */
BEGIN
  DECLARE  @errno   int,
           @errmsg  varchar(255)
    /* ERwin Builtin Trigger */
    /* ����  ���������� on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="00037912", PARENT_OWNER="", PARENT_TABLE="����"
    CHILD_OWNER="", CHILD_TABLE="����������"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_14", FK_COLUMNS="subdivision_number""position_number" */
    IF EXISTS (
      SELECT * FROM deleted,����������
      WHERE
        /*  %JoinFKPK(����������,deleted," = "," AND") */
        ����������.subdivision_number = deleted.subdivision_number AND
        ����������.position_number = deleted.position_number
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete ���� because ���������� exists.'
      GOTO ERROR
    END

    /* ERwin Builtin Trigger */
    /* �������������  ���� on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="�������������"
    CHILD_OWNER="", CHILD_TABLE="����"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_5", FK_COLUMNS="subdivision_number" */
    IF EXISTS (SELECT * FROM deleted,�������������
      WHERE
        /* %JoinFKPK(deleted,�������������," = "," AND") */
        deleted.subdivision_number = �������������.subdivision_number AND
        NOT EXISTS (
          SELECT * FROM ����
          WHERE
            /* %JoinFKPK(����,�������������," = "," AND") */
            ����.subdivision_number = �������������.subdivision_number
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last ���� because ������������� exists.'
      GOTO ERROR
    END

    /* ERwin Builtin Trigger */
    /* ���������  ���� on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="���������"
    CHILD_OWNER="", CHILD_TABLE="����"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_6", FK_COLUMNS="position_number" */
    IF EXISTS (SELECT * FROM deleted,���������
      WHERE
        /* %JoinFKPK(deleted,���������," = "," AND") */
        deleted.position_number = ���������.position_number AND
        NOT EXISTS (
          SELECT * FROM ����
          WHERE
            /* %JoinFKPK(����,���������," = "," AND") */
            ����.position_number = ���������.position_number
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last ���� because ��������� exists.'
      GOTO ERROR
    END


    /* ERwin Builtin Trigger */
    RETURN
ERROR:
    raiserror @errno @errmsg
    rollback transaction
END

go


CREATE TRIGGER tU_���� ON ���� FOR UPDATE AS
/* ERwin Builtin Trigger */
/* UPDATE trigger on ���� */
BEGIN
  DECLARE  @NUMROWS int,
           @nullcnt int,
           @validcnt int,
           @inssubdivision_number integer, 
           @insposition_number integer,
           @errno   int,
           @errmsg  varchar(255)

  SELECT @NUMROWS = @@rowcount
  /* ERwin Builtin Trigger */
  /* ����  ���������� on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="00040242", PARENT_OWNER="", PARENT_TABLE="����"
    CHILD_OWNER="", CHILD_TABLE="����������"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_14", FK_COLUMNS="subdivision_number""position_number" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(subdivision_number) OR
    UPDATE(position_number)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,����������
      WHERE
        /*  %JoinFKPK(����������,deleted," = "," AND") */
        ����������.subdivision_number = deleted.subdivision_number AND
        ����������.position_number = deleted.position_number
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update ���� because ���������� exists.'
      GOTO ERROR
    END
  END

  /* ERwin Builtin Trigger */
  /* �������������  ���� on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="�������������"
    CHILD_OWNER="", CHILD_TABLE="����"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_5", FK_COLUMNS="subdivision_number" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(subdivision_number)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,�������������
        WHERE
          /* %JoinFKPK(inserted,�������������) */
          inserted.subdivision_number = �������������.subdivision_number
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    
    IF @validcnt + @nullcnt != @NUMROWS
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update ���� because ������������� does not exist.'
      GOTO ERROR
    END
  END

  /* ERwin Builtin Trigger */
  /* ���������  ���� on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="���������"
    CHILD_OWNER="", CHILD_TABLE="����"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_6", FK_COLUMNS="position_number" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(position_number)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,���������
        WHERE
          /* %JoinFKPK(inserted,���������) */
          inserted.position_number = ���������.position_number
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    
    IF @validcnt + @nullcnt != @NUMROWS
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update ���� because ��������� does not exist.'
      GOTO ERROR
    END
  END


  /* ERwin Builtin Trigger */
  RETURN
ERROR:
    raiserror @errno @errmsg
    rollback transaction
END

go

